﻿using System.Collections.Generic;
using ElectronicJaw.Toonscapes.Content.Level;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace ElectronicJaw.Toonscapes.UpdatedBoard.DamageSystem
{
    [Serializable]
    public class DamageController : IEventEmitter
    {
        [SerializeReference] private BoardData board = default;

        public async Task DamageCells(IEnumerable<Cell> cells, IDamage damage)
        {
            var tasks = new List<Task>();
            List<Cell> taskTiles = new List<Cell>(cells);
            board.BoardStatus.UpdateTasks(taskTiles);

            foreach (var cell in cells)
            {
                if (cell.Item is Booster)
                    tasks.Add((cell.Item as Booster).UseBooster(board, default, DamageCells));

                tasks.Add(cell.DamageCell(damage));
            }
            
            await Task.WhenAll(tasks);           
        }

        public async Task SideDamageCells(IEnumerable<Cell> cells, SideDamage damage)
        {
            var directions = new DirectionsSet(new List<Direction>()
                    {
                        Direction.Up,
                        Direction.Down,
                        Direction.Left,
                        Direction.Right
                    });

            foreach (var cell in cells)
            {
                var targetCells = board.FindCellNeighbours(cell, directions);

                foreach (var targetCell in targetCells)
                    await targetCell.DamageCell(damage);
            }
            
        }

    }
}
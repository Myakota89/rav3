﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using ElectronicJaw.Toonscapes.Content.Level;
using ElectronicJaw.Toonscapes.Controllers.Spawn;
using UnityEngine;

namespace ElectronicJaw.Toonscapes.UpdatedBoard
{
    public class FallController : MonoBehaviour
    {
        [SerializeField] private BoardData board = default;
        public bool IsActive { get; private set; }
        public bool IsFrozen { get; private set; }
        public Task FreezeTask { get; private set; }
        
        public event Action Unfrozen;
        
        public async void FreezeLoop(Task freezeTask)
        {
            if (!IsActive)
                throw new OperationCanceledException("Unable to freeze inactive fall controller!");
            
            //It is hack to stop dual freeze problem - it can be unfreezed early than needed
            if (IsFrozen)
                throw new OperationCanceledException("Already frozen");
            
            IsFrozen = true;
            FreezeTask = freezeTask;
            
            await freezeTask;
            
            FreezeTask = null;
            IsFrozen = false;
            
            Unfrozen?.Invoke();
        }
        
        public async Task PerformFall()
        {
            await StartCellsDrop();
        }
        
        public async Task StartCellsDrop()
        {
            var tasks = new List<Task>();
            
            for (int y = board.Cells.Height - 1; y > -1; y--)
            {
                for (int x = 0; x < board.Cells.Width; x++)
                {
                    var cell = board.Cells[x, y];
                    
                    if (cell.ItemHandler != default && cell.ItemHandler.NeedsItemHandle)
                        tasks.Add(HandleCell(cell));

                    if (cell.Restrain.IsActive || !cell.IsExist)
                        FallUnderRestrain(new Vector2Int(x, y));


                    if (!cell.Item.IsExist) continue;

                    if (CellCanFall(new Vector2Int(x, y)))
                    {
                        if (cell.Item.CanBeAnimated)
                        {
                            cell.Item.MarkAsDynamic();
                            tasks.Add(DropCell(cell));                           
                        }
                    }
                    else
                    {
                        cell.Item.MarkAsNotDynamic();
                        cell.Item.View.ResetSpeed();                        
                    }
                }
            }
            
            await Task.WhenAll(tasks);
        }

        public bool CellCanFall(Vector2Int position) =>
            !EmptyCell(position) && 
            position.y < board.Cells.Height - 1 && 
            CellFallTarget(position) != default;


        public Cell CellFallTarget(Vector2Int position) =>
             EmptyCell(position + Vector2Int.up) ? board[position + Vector2Int.up] : CellDiagonalFallTarget(position);

        private bool EmptyCell(Vector2Int position) =>
            board[position].IsExist && 
            !board[position].Item.IsExist && 
            !board[position].Restrain.IsActive;


        private Cell CellDiagonalFallTarget(Vector2Int position)
        {
            if (!board[position].Item.IsDynamicState)
                return default;

            Cell cell = default;

            if (CanCellDiagonalFall(position, Vector2Int.left))
                cell = board[position + Vector2Int.up + Vector2Int.left];
            else if (CanCellDiagonalFall(position, Vector2Int.right))
                cell = board[position + Vector2Int.up + Vector2Int.right];

            return cell;
        }

        private bool CanCellDiagonalFall(Vector2Int position, Vector2Int direction)
        {
            var fallTargetPosition = position + direction + Vector2Int.up;
            if (fallTargetPosition.x < 0 || fallTargetPosition.x > board.Cells.Width - 1 || !EmptyCell(fallTargetPosition))
                return false;

            var result = true;
            var cell = board[position + direction + Vector2Int.up];            
            var boardCells = board.FindAll(boardCell =>
                cell.Position.x == boardCell.Position.x && cell.Position.y > boardCell.Position.y).ToList();
            
            boardCells.Sort((item1, item2) =>
            {
                if (item1.Position.y == item2.Position.y)
                    return 0;
                if (item1.Position.y < item2.Position.y)
                    return 1;
                else
                    return -1;
            });
            
            foreach (var boardCell in boardCells)
            {
                if (boardCell.Restrain.IsActive || !boardCell.IsExist)
                    break;

                if (CellCanFall(boardCell.Position))
                    return false;
            }
            
            return result;
        }

        private void FallUnderRestrain(Vector2Int position)
        {
            if (position.y == board.Cells.Height - 1)
                return;

            if (position.x > 0)
                board[position + Vector2Int.left].Item.MarkAsDynamic();
            if (position.x < board.Cells.Width - 1)
                board[position + Vector2Int.right].Item.MarkAsDynamic();


        }

        public async Task HandleCell(Cell cell)
        {
            await cell.ItemHandler.HandleItem();
            await StartCellsDrop();
        }
        
        public async Task DropCell(Cell cell)
        {
            if (FreezeTask?.Status == TaskStatus.Running)
            {
                cell.Item.View.ResetSpeed();
                await FreezeTask;
            }
            
            if (!CellCanFall(cell.Position))
                throw new OperationCanceledException("Cell can not fall");
            
            var fallTarget = CellFallTarget(cell.Position);
            
            var item = cell.ReleaseItem();
            item.View.IncreaseSpeed();
            
            await fallTarget.SetNewItem(item);
            await StartCellsDrop();
        }
    }
}

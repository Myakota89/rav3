﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ElectronicJaw.Toonscapes.Content.Level;
using ElectronicJaw.Toonscapes.Content.User;
using ElectronicJaw.Toonscapes.UpdatedBoard.DamageSystem;
using UnityEngine;

namespace ElectronicJaw.Toonscapes.UpdatedBoard
{
    public class TurnController : MonoBehaviour
    {
        [SerializeField] private FallController _fallController = default;
        [SerializeField] private CombinationsController _combinationsController = default;
        [SerializeField] private DamageController damageController = default;
        [SerializeField] private BoosterController boosterController = default;
        [SerializeField] private BoardData _board = default;
        [SerializeField] private BoosterCombinationsTable boosterCombinationsTable = default;
        public event Action OnUseBooster;

        public async Task PerformTurn(Cell firstCell, Cell secondCell)
        {
            if (CombinableBoosters(firstCell.Item as Booster, secondCell?.Item as Booster))
            {
                var combination = boosterCombinationsTable.GetBoosterCombination(firstCell.Item as Booster, secondCell.Item as Booster);
                firstCell.DestroyItem();
                secondCell.DestroyItem();
                await UseBooster(combination.combinationHandler, secondCell);
            }
            else if (firstCell.Item is Booster)
                await UseBooster(firstCell.Item as Booster, secondCell);                              
            else if (secondCell.Item is Booster)
                await UseBooster(secondCell.Item as Booster, firstCell);
                
            await _fallController.PerformFall();
            await GameTurn();
            firstCell?.MarkAsNotSwitchable();
            secondCell?.MarkAsNotSwitchable();
        }

        public async Task GameTurn()
        {
            while (BoardContainsCombinations())
            {
                var combinations = _combinationsController.GetCombinations(_board);
                var combinedCells = combinations.SelectMany(combination => combination.Cells);                            

                await damageController.SideDamageCells(combinedCells, new SideDamage(1, DamageType.NextToExplosion, (item, damageData) => {}));
                await damageController.DamageCells(combinedCells, new CombinationDamage(1, DamageType.PrimaryExplosion, (item, damageData) => {}));
                boosterController.CreateBooster(combinations, _board);

                await _fallController.PerformFall();
            }

            _board.UpdateRestrainSystems();
        }

        

        private async Task UseBooster(IBooster booster, Cell secondCell)
        {
            OnUseBooster();
            await booster.UseBooster(_board, secondCell, damageController.DamageCells);
        }
            
        private bool CombinableBoosters(Booster firstBooster, Booster secondBooster) =>
            !(firstBooster is InventoryBooster) && firstBooster is Booster && secondBooster is Booster;

        public bool BoardContainsCombinations() =>
            GetCombinations().Any();

        public IEnumerable<Combination> GetCombinations() =>
            _combinationsController.GetCombinations(_board);

        public bool BestCombination(Combination firstCombination, Combination secondCombination) =>
            _combinationsController.BestCombination(firstCombination, secondCombination);
    }
}
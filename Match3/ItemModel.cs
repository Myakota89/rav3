﻿using UnityEngine;

namespace ElectronicJaw.Toonscapes.Content.Level
{
    public abstract class ItemModel : ScriptableObjectModel<Item>
    {
        public abstract bool IsDamageable { get; }
        
        [SerializeField] private Sprite _sprite = null;
        public Sprite Sprite => _sprite;
        
        [SerializeField]
        Vector2 _size = Vector2.zero;
        public Vector2 Size => _size;
    }
}

﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using ElectronicJaw.Toonscapes.Content.Restrains;
using ElectronicJaw.Toonscapes.UpdatedBoard.DamageSystem;

namespace ElectronicJaw.Toonscapes.Content.Level
{
    public class Item
    {
        public ItemModel Model { get; private set; }
        public Restrain Restrain { get; private set; } = new Restrain();

        private Cell parentCell = default;
        public Vector2Int Position => parentCell.Position;
        public bool IsDynamicState { get; private set; } = false;     
        public bool CanBeAnimated => IsExist && !IsAnimating;
        public bool IsAnimating { get; private set; } = false;
        public bool IsExist => Model != default;
        public bool IsDamageable => Model.IsDamageable;

        protected bool isDoomed = false;

        public bool IsMovable
        {
            get
            {
                if (Model is GemModel model)
                    if (model.GemType == GemType.None && Restrain == null)
                        return false;
                
                return true;
            }
        }

        public bool IsClickable
        {
            get
            {
                bool result = true;
                result &= IsExist;
                
                if (Model is GemModel model)
                {
                    result &= model.GemType != GemType.None;
                    result &= !Restrain.HasTag<IgnoreSelectionTag>();
                }
                
                return result;
            }
        }
        
        public ItemView View { get; set; }

        public Item(ItemView view, ItemModel model)
        {
            View = view;
            Model = model;
        }

        public Item(ItemModel model)
        {
            Model = model;
        }

        public Item() {}

        public virtual bool IsCombinableWith(Item item) =>
            false;

        public void SetView(ItemView view)
        {
            if (View != null)
                throw new InvalidOperationException();
            
            View = view;
        }

        public void SetRestrain(Restrain restrain)
        {
            if (Restrain.IsActive)
                Restrain.Destroy();

            Restrain = restrain;
            Restrain.AttachViewCarrier(View.gameObject);
        }

        public void SetParentCell(Cell cell) =>
            parentCell = cell;

        public async Task MoveToHisCell()
        {
            MarkAsAnimating();
            await View.MoveToCenter();
            MarkAsAnimated();
        }

        public void MarkAsAnimating() =>
            IsAnimating = true;

        public void MarkAsAnimated() =>
            IsAnimating = false;

        public void MarkAsDynamic() =>
            IsDynamicState = true;

        public void MarkAsNotDynamic() =>
            IsDynamicState = false;

        public void MarkAsDoomed(bool mark = true) =>
            isDoomed = mark;

        #region move
        public async Task SwitchCell(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);
            
            await View.SwitchCell(position);
            MarkAsAnimated();
        }

        public async Task SwitchBack(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);
            
            await View.SwitchBack(position);
            MarkAsAnimated();
        }

        public async Task FlipCell(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);
            
            await View.FlipCell(position);
            MarkAsAnimated();
        }

        public async Task FlipBack(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);
            
            await View.FlipBack(position);
            MarkAsAnimated();
        }

        public async Task MergeCell(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);

            await View.MergeCell(position);
            MarkAsAnimated();
        }

        public async Task MergeBack(Cell cell)
        {
            MarkAsAnimating();
            Vector2 position = new Vector2(cell.HorizontalPosition, -cell.VerticalPosition);

            await View.MergeBack(position);
            MarkAsAnimated();
        }
        #endregion


        public async Task DamageItem(IDamage damage)
        {
            if (View)
            {
                if (!View.isDoomed)
                {
                    View.isDoomed = true;
                    if (!IsAnimating)
                    {
                        MarkAsAnimating();
                        await View.DamageAnimation(damage);
                        MarkAsAnimated();
                    }
                    DestroyView();
                }
            }
        }
        
        public void DestroyView()
        {
            if (View)
            {
                View.Destroy();
                View = null;
            }
        }
    }
}

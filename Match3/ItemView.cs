﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using ElectronicJaw.Toonscapes.UI.Game;
using ElectronicJaw.Toonscapes.UpdatedBoard;
using ElectronicJaw.Toonscapes.UpdatedBoard.DamageSystem;
using System;

namespace ElectronicJaw.Toonscapes.Content.Level
{
    public class ItemView : MonoBehaviour
    {
        public MoveAnimationSettings MoveConfig => moveAnimationSettings;
        [SerializeField] private MoveAnimationSettings moveAnimationSettings = default;
        public PrimaryDamageAnimationConfig DamageAnimationConfig => primaryAnimationConfig;
        [SerializeField] private PrimaryDamageAnimationConfig primaryAnimationConfig = default;
        [SerializeField] private NextToDamageAnimationConfig nextToDamageAnimationConfig = default;
        private SpriteRenderer spriteRenderer;
        private float moveTime = 0.2f;
        private Animation unityAnimation = default;
        private bool selfDestroy = false;

        public bool isDoomed;

        public float CurrentSpeed { get; private set; } = 1f;

        public void IncreaseSpeed() =>
            CurrentSpeed *= 1.25f;

        public void ResetSpeed() =>
            CurrentSpeed = 1f;
        
        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            unityAnimation = GetComponent<Animation>();
        }

        public void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

        public void ChangeSpriteOrder(int value)
        {
            spriteRenderer.sortingOrder += value;
        }

        public void SetHintSprite(Sprite sprite) =>
            hintSR.sprite = sprite;

        public void Destroy()
        {
            if (selfDestroy) return;
            Destroy(gameObject);
        }

        public void GetReadyFall(Vector2 startPosition)
        {
            transform.position = startPosition;
        }

        #region move
        public Task SwitchCell(Vector2 destination)
        {
            transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset;
            return transform.DOMove(destination, moveAnimationSettings.swapTime)
                .SetEase(moveAnimationSettings.swapEase).AsyncWaitForCompletion();
        }
        
        public Task SwitchBack(Vector2 destination)
        {
            return transform.DOMove(destination, moveAnimationSettings.swapTime)
                .SetEase(moveAnimationSettings.swapBackEase).AsyncWaitForCompletion();
        }

        public Task FlipCell(Vector2 destination)
        {
            var duration = moveAnimationSettings.flipTime;
            var inTime = duration * moveAnimationSettings.flipInOutDestribution;
            var outTime = duration - inTime;
            var startPos = transform.position;

            transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset;
            var flipSequence = DOTween.Sequence();
            return flipSequence.
                Append(transform.DOMove(destination, inTime).SetEase(moveAnimationSettings.flipInEase)).
                Append(transform.DOMove(startPos, outTime).SetEase(moveAnimationSettings.flipOutEase)).
                AsyncWaitForCompletion();
        }

        public Task FlipBack(Vector2 destination)
        {
            var duration = moveAnimationSettings.flipTime;
            var inTime = duration * moveAnimationSettings.flipInOutDestribution;
            var outTime = duration - inTime;
            var hideTime = inTime * moveAnimationSettings.backflipHideShowDestribution;
            var showTime = inTime - hideTime;
            var startPos = transform.position;

            var backflipSequence = DOTween.Sequence();
            return backflipSequence.
                Append(transform.DOMove(0.5f * (startPos + (Vector3) destination), hideTime).SetEase(moveAnimationSettings.backflipHideEase)).
                Append(transform.DOMove(destination, showTime).SetEase(moveAnimationSettings.backflipShowEase).OnComplete(() => 
                    transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset)).
                Append(transform.DOMove(startPos, outTime).SetEase(moveAnimationSettings.backflipOutEase)).
                AsyncWaitForCompletion();
        }

        public Task MergeCell(Vector2 destination)
        {
            var phaseTime = moveAnimationSettings.mergeTime / 2.5f;
            var startPos = transform.position;
            var midPos = Vector3.Lerp(startPos, destination, moveAnimationSettings.mergeFirstGemBias);

            Instantiate(moveAnimationSettings.mergeEffect, destination, Quaternion.identity);
            transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset;
            transform.DOScale(moveAnimationSettings.mergeScale, moveAnimationSettings.mergeTime).SetEase(moveAnimationSettings.mergeScaleEase).OnComplete(() => transform.localScale = Vector3.one);
            var mergeSequence = DOTween.Sequence();
            return mergeSequence.
                Append(transform.DOMove(destination, phaseTime).SetEase(moveAnimationSettings.flipInEase)).
                Append(transform.DOMove(midPos, phaseTime).SetEase(moveAnimationSettings.mergeMidEase)).
                Append(transform.DOMove(destination, phaseTime * 0.5f).SetEase(moveAnimationSettings.flipOutEase)).
                AsyncWaitForCompletion();
        }

        public Task MergeBack(Vector2 destination)
        {
            var phaseTime = moveAnimationSettings.mergeTime / 2.5f;
            var startPos = transform.position;
            var halfPos = Vector3.Lerp(startPos, destination, 0.5f);
            var midPos = Vector3.Lerp(startPos, destination, -moveAnimationSettings.mergeSecondGemBias);

            transform.DOScale(moveAnimationSettings.mergeScale, moveAnimationSettings.mergeTime).SetEase(moveAnimationSettings.mergeScaleEase).OnComplete(() => transform.localScale = Vector3.one);
            var backmergeSequence = DOTween.Sequence();
            return backmergeSequence.
                Append(transform.DOMove(halfPos, phaseTime * 0.5f).SetEase(moveAnimationSettings.backflipHideEase)).
                Append(transform.DOMove(destination, phaseTime * 0.5f).SetEase(moveAnimationSettings.backflipShowEase).OnComplete(() =>
                    transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset)).
                Append(transform.DOMove(midPos, phaseTime).SetEase(moveAnimationSettings.mergeMidEase)).
                Append(transform.DOMove(startPos, phaseTime * 0.5f).SetEase(moveAnimationSettings.backflipOutEase)).
                AsyncWaitForCompletion();
        }
        #endregion

        #region hint
        [Header("Hint"), SerializeField] private SpriteRenderer hintSR = null;
        private Sequence hintMoveSequence;
        private Sequence hintGlowSequence;

        public Task ShowHint(Direction direction)
        {
            var dir = VectorFromDirection(direction);
            var showTime = moveAnimationSettings.hintTime * 0.5f;
            var returnTime = moveAnimationSettings.hintTime * (1f - moveAnimationSettings.hintMoveReturn) * 0.5f;
            var pauseTime = moveAnimationSettings.hintPauseTime;
            
            var animations = new List<Task>();
            
            if (direction != Direction.None)
            {
                hintMoveSequence = DOTween.Sequence();
                animations.Add(hintMoveSequence.
                    Append(transform.DOLocalMove(dir * moveAnimationSettings.hintMoveDistance + Vector3.forward * moveAnimationSettings.aboveTileZoffset, showTime).SetEase(Ease.InOutSine)).
                    Join(hintSR.DOFade(1f, showTime).SetEase(Ease.InOutSine)).
                    Append(transform.DOLocalMove(dir * moveAnimationSettings.hintMoveDistance * moveAnimationSettings.hintMoveReturn + Vector3.forward * moveAnimationSettings.aboveTileZoffset, returnTime).SetEase(Ease.InOutSine)).
                    Join(hintSR.DOFade(moveAnimationSettings.hintReturnOpacity, returnTime).SetEase(Ease.InOutSine)).
                    Append(transform.DOLocalMove(dir * moveAnimationSettings.hintMoveDistance + Vector3.forward * moveAnimationSettings.aboveTileZoffset, returnTime).SetEase(Ease.InOutSine)).
                    Join(hintSR.DOFade(1f, returnTime).SetEase(Ease.InOutSine)).
                    Append(transform.DOLocalMove(Vector3.forward * moveAnimationSettings.aboveTileZoffset, showTime).SetEase(Ease.InOutSine)).
                    Join(hintSR.DOFade(0f, showTime).SetEase(Ease.InOutSine)).
                    Append(hintSR.DOFade(0f, pauseTime)).
                    SetLoops(-1).AsyncWaitForCompletion());
            }
            
            hintGlowSequence = DOTween.Sequence();
            animations.Add(hintGlowSequence
                .Append(hintSR.DOFade(1f, showTime)
                    .SetEase(Ease.InOutSine))
                .Append(hintSR.DOFade(moveAnimationSettings.hintReturnOpacity, returnTime)
                    .SetEase(Ease.InOutSine))
                .Append(hintSR.DOFade(1f, returnTime)
                    .SetEase(Ease.InOutSine))
                .Append(hintSR.DOFade(0f, showTime)
                    .SetEase(Ease.InOutSine))
                .Append(hintSR.DOFade(0f, pauseTime))
                .SetLoops(-1)
                .AsyncWaitForCompletion());
            
            return Task.WhenAll(animations);
        }

        public void HideHint()
        {
            if (hintMoveSequence != null) hintMoveSequence.Kill();
            if (hintGlowSequence != null) hintGlowSequence.Kill();
            if (hintSR != null) hintSR.color = new Color(1f, 1f, 1f, 0f);
            transform.localPosition = Vector3.zero;
        }
        #endregion

        public Task MoveToCenter() =>
            transform.DOLocalMove(Vector3.zero, moveTime / CurrentSpeed).SetEase(Ease.Linear).AsyncWaitForCompletion();

        private Vector3 VectorFromDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.None:
                    return Vector3.zero;
                case Direction.Left:
                    return Vector3.left;
                case Direction.Right:
                    return Vector3.right;
                case Direction.Up:
                    return Vector3.up;
                case Direction.Down:
                    return Vector3.down;
                default:
                    Debug.LogError("unexpected direction");
                    return Vector3.zero;
            }
        }

        #region damage
        public struct CollectInfo
        {
            public bool isCollectable;
            public LevelGoalView target;
            public int index;
            public Action callback;
            public Cell cellToTransform;

            public CollectInfo(bool isCollectable = false, LevelGoalView target = null, int index = 0, Action callback = null, Cell cellToTransform = null)
            {
                this.isCollectable = isCollectable;
                this.target = target;
                this.index = index;
                this.callback = callback;
                this.cellToTransform = cellToTransform;
            }
        }

        public CollectInfo collectInfo = new CollectInfo();

        public async Task DamageAnimation(IDamage damage)
        {
            if (damage.Type == DamageType.InstantDamage)
            {
                Highlight();
                return;
            }
            switch (damage)
            {
                case BoosterDamage booster:
                    await booster.AnimateItemDamage(this);
                    if (collectInfo.isCollectable)
                    {
                        Highlight();
                        await SimpleCollectAnimation();
                    }
                    //else await booster.AnimateItemDamage(this);
                    break;
                case CombinationDamage comb:
                    await PrimaryDamageAnimation(comb.Combination);
                    break;
            }
            collectInfo.isCollectable = false;
        }

        private async Task PrimaryDamageAnimation(Combination combination)
        {
            if (combination.Model.BoosterModel)
            {
                await CombineAnimation(combination.SwitchableCell.Item.View.transform.position, combination.SwitchableCell.Item.View == this);
            }
            else
            {
                if (collectInfo.isCollectable) await SimpleCollectAnimation();
                else await DestroyAnimation();
            }
        }
        
        private Task DestroyAnimation()
        {
            var config = primaryAnimationConfig;
            var effect = config.tileDestroyEffect;
            if (effect != null) Instantiate(effect, transform.position, Quaternion.identity);
            return transform.DOScale(Vector3.zero, config.tileDestroyTime).SetEase(config.tileDestroyEase).AsyncWaitForCompletion();
        }

        private async Task SimpleCollectAnimation()
        {
            var config = primaryAnimationConfig;
            var target = collectInfo.target.transform.position;
            transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset * collectInfo.index;

            var sequence = DOTween.Sequence();
            if (spriteRenderer.sprite == null) CollectEnd();
            else
            {

                if (transform.localScale.x > 0.8f)
                {
                    await sequence.
                        Append(transform.DOScale(Vector3.one * config.collectGrowthSize, config.tileDestroyTime * 0.5f).SetEase(Ease.OutQuad)).
                        Append(transform.DOScale(Vector3.one * config.collectFlySize, config.tileDestroyTime * 0.5f).SetEase(Ease.InQuad)).
                        AsyncWaitForCompletion();
                }
                else
                {
                    transform.localEulerAngles = Vector3.zero;
                    transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset * collectInfo.index;
                    var growthTime = config.collectBonusGrowthTime + collectInfo.index * config.collectFlyChainDelay;
                    transform.DOScale(Vector3.one * config.collectFlySize, growthTime).SetEase(Ease.OutQuad);
                }
                CollectFlyAnimation();
            }
        }

        private async Task CombineAnimation(Vector3 endPosition, bool isCentral)
        {
            var config = primaryAnimationConfig;

            var gatherTime = config.combineGatherTime;
            var duration = gatherTime + config.combineAftershakeTime;
            var afterTime = config.combineAftershakeTime;
            var scaleInTime = afterTime * config.combineAftershakeInOutDistribution;
            var scaleOutTime = afterTime - scaleInTime;

            var dir = endPosition - transform.position;
            dir.z = 0f;
            var sequence = DOTween.Sequence();

            Highlight();
            if (isCentral)
            {
                transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset;
                Instantiate(config.bonusFlarePrefab, transform.position, Quaternion.identity);

                sequence.
                    Append(transform.DOScale(Vector3.one * config.combineGrowthScale, gatherTime).SetEase(config.combineGrowthEase)).
                    Append(transform.DOScale(Vector3.one * config.combineAftershakeScale, scaleInTime).SetEase(config.combineAftershakeInEase)).
                    Append(transform.DOScale(Vector3.one, scaleOutTime).SetEase(config.combineAftershakeOutEase)).
                    OnComplete(() =>
                    {
                        transform.position -= Vector3.forward * moveAnimationSettings.aboveTileZoffset;
                        transform.localScale = Vector3.zero;
                    });
            }
            else
            {
                var dust = Instantiate(config.bonusDustPrefab, endPosition, Quaternion.identity).GetComponent<ParticleSystem>();
                var shapeModule = dust.shape;
                shapeModule.position = -dir;

                sequence.
                    Append(transform.DORotate(config.combineSqueezeAngle * dir.normalized, gatherTime).SetEase(Ease.OutQuart)).
                    Join(transform.DOMove(endPosition, gatherTime).SetEase(config.combineGatherEase).OnComplete(() => transform.localScale = Vector3.zero));
            }
            await transform.DOScaleZ(1f, duration).AsyncWaitForCompletion();

            if (collectInfo.isCollectable)
            {
                var target = collectInfo.target.transform.position;
                transform.localEulerAngles = Vector3.zero;
                transform.position += Vector3.forward * moveAnimationSettings.aboveTileZoffset * collectInfo.index;
                var growthTime = config.collectBonusGrowthTime + collectInfo.index * config.collectFlyChainDelay;
                transform.DOScale(Vector3.one * config.collectFlySize, growthTime).SetEase(Ease.OutQuad);
                CollectFlyAnimation();
            }
        }

        private void CollectFlyAnimation()
        {
            selfDestroy = true;
            spriteRenderer.sortingLayerName = "UI";
            var config = primaryAnimationConfig;
            var target = collectInfo.target.transform.position;
            var flyTime = config.collectFlyTime + collectInfo.index * config.collectFlyChainDelay;

            var sequence = DOTween.Sequence();
            sequence.
                Append(transform.DOMoveX(target.x, flyTime).SetEase(config.collectFlyEaseX)).
                Join(transform.DOMoveY(target.y, flyTime).SetEase(config.collectFlyEaseY)).
                Append(transform.DOScale(Vector3.one * config.collectFinalSize, config.collectSrinkTime).SetEase(config.collectSrinkEase)).
                OnComplete(() => CollectEnd());
        }

        private void CollectEnd()
        {
            collectInfo.callback();
            selfDestroy = false;
            Destroy();
        }

        public void Highlight()
        {
            Instantiate(primaryAnimationConfig.tileHLPrefab, transform.position, Quaternion.identity);
        }
        #endregion

        public async Task CreateBoosterAnimation()
        {
            unityAnimation.Play(UnityAnimation.BoosterCreate.ToString());
            await Task.Delay(Mathf.RoundToInt(1000f * primaryAnimationConfig.combineBoosterFallDelay));
        }

        private enum UnityAnimation
        {
            BoosterCreate
        }
    }
}
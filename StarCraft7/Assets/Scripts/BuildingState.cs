﻿public enum BuildingState
{
        
    ShopSelection,
    Created,
    Constructing,
    Built,
    Mining
}
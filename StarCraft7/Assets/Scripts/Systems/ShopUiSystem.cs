﻿using System;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Actions;
using Leopotam.Ecs.Ui.Components;
using Leopotam.Ecs.Ui.Systems;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UIElements;

namespace Systems
{
    public class ShopUiSystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsUiNamed("ShopWindow")] private GameObject shopWindow;
        [EcsUiNamed("OpenShopBtn")] private GameObject shopOpenBtn;
        private EcsUiEmitter uiEmitter = default;
        private EcsWorld world = default;
        
        private EcsFilter<EcsUiClickEvent> clickFilter = default;
        private EcsFilter<InputComponent> inputFilter = default;
        private EcsFilter<GameStateComponent> gameStateFilter = default;

        public void Init()
        {
            SetStateShopWindow(false);
        }
        
        public void Run()
        {

            CheckUiState();

            foreach (var i in clickFilter)
            {
                var data = clickFilter.Get1(i);
                ButtonHandle(data);
            }
        }

        private void ButtonHandle(EcsUiClickEvent buttonData)
        {
            switch (buttonData.WidgetName)
            {
                case "OpenShopBtn":
                    SetStateShopWindow(true);
                    break;
                
                case "ExitShopBtn": 
                    SetStateShopWindow(false);
                    break;
                
                case "BuildingGasStorage":
                    case "BuildingGasMiner":
                        case "BuildingMineralsStorage":
                            case "BuildingMineralsMiner":
                    CreateBuilding(buttonData.Sender);
                    break;
            }
        }

        private void SetStateShopWindow(bool state) =>
            shopWindow.SetActive(state);

        private void SetStateShopOpenBtn(bool state) =>
            shopOpenBtn.SetActive(state);

        private void CreateBuilding(GameObject sender)
        {
            if (gameStateFilter.Get1(0).state != GameState.Free)
                return;

            var buildingEmitter = sender.GetComponent<BuildingButton>();
            var buildingEntity = world.NewEntity();
            buildingEntity.Get<BuildingComponent>().type = buildingEmitter.BuildingType;
            buildingEntity.Get<BuildingComponent>().name = buildingEmitter.BuildingName;
            buildingEntity.Get<BuildingViewComponent>();
            buildingEntity.Get<TimeComponent>();
            buildingEntity.Get<ColliderComponent>();

            inputFilter.Get1(0).isClick = false;
            gameStateFilter.Get1(0).state = GameState.Constructing;
        }

        private void CheckUiState()
        {
            if (gameStateFilter.Get1(0).state == GameState.Constructing)
            {
                SetStateShopWindow(false);
                SetStateShopOpenBtn(false);
            }
            else
                SetStateShopOpenBtn(true);
        }
    }
}
﻿using System;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class BuildSystem : IEcsRunSystem
    {
        private readonly EcsFilter<GameStateComponent> sessionEntity = default;
        private readonly EcsFilter<BuildingComponent, BuildingViewComponent, TimeComponent> buildingEntities = default;

        public void Run()
        {
            foreach (var i in buildingEntities)
            {
                if (buildingEntities.Get1(i).state != BuildingState.Constructing)
                    continue;

                ref var buildingComponent = ref buildingEntities.Get1(i);
                ref var viewComponent = ref buildingEntities.Get2(i);
                ref var timeComponent = ref buildingEntities.Get3(i);

                if (timeComponent.constructingTimer.TotalSeconds < 1)
                {
                    SetBuildingState(ref buildingComponent, ref timeComponent);
                    sessionEntity.Get1(0).state = GameState.Free;
                }
                else
                {
                    viewComponent.view.SetTimerText(timeComponent.constructingTimer.ToString());
                    viewComponent.view.SetTimerColor(Color.yellow);
                }
            }
        }

        private void SetBuildingState(ref BuildingComponent buildingComponent, ref TimeComponent timeComponent)
        {
            switch (buildingComponent.type)
            {
                case BuildingType.Miner:
                    buildingComponent.state = BuildingState.Mining;
                    timeComponent.miningTimer = timeComponent.miningTime;
                    break;
                case BuildingType.Storage:
                    buildingComponent.state = BuildingState.Built;
                    break;
            }
        }
    }
}
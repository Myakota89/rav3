﻿using Components;
using Leopotam.Ecs;

namespace Systems
{
    public class ResourceSystem : IEcsRunSystem
    {
        private EcsFilter<GameResourcesComponent> sessionEntity = default;
        
        public void Run()
        {
            ref var resources = ref sessionEntity.Get1(0);
            resources.gas = resources.gas > resources.maxAmountGas ? resources.maxAmountGas : resources.gas;
            resources.minerals = resources.minerals > resources.maxAmountMinerals ? resources.maxAmountMinerals : resources.minerals;

            resources.gasFilled = resources.gas == resources.maxAmountGas;
            resources.mineralsFilled = resources.minerals == resources.maxAmountMinerals;
        }
    }
}
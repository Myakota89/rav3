﻿using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class CameraMoveSystem : IEcsRunSystem
    {
        [EcsUiNamed("ShopWindow")] private GameObject shopWindow;
        private readonly CameraSettings settings = default;
        private readonly EcsFilter<CameraComponent> cameraEntity = default;
        private readonly EcsFilter<InputComponent> inputEntity = default;
        private readonly EcsFilter<GameStateComponent> sessionEntity = default;
        
        
        public void Run()
        {
            if (shopWindow.activeSelf)
                return;

            ref var cameraComponent = ref cameraEntity.Get1(0);
            var inputComponent = inputEntity.Get1(0);

            if (inputComponent.isDrag)
            {
                if (sessionEntity.Get1(0).state == GameState.Constructing)
                    return;

                var differencePosition = (inputComponent.clickPosition - inputComponent.inputPosition) * settings.CameraMoveSpeed;
                var newCameraPosition = new Vector3(differencePosition.x, 0, differencePosition.y) + 
                                        new Vector3(cameraComponent.lastCameraPosition.x, 0, cameraComponent.lastCameraPosition.z);
                
                float posX = Mathf.Clamp(newCameraPosition.x, settings.BorderMinX, settings.BorderMaxX);
                float posZ = Mathf.Clamp(newCameraPosition.z, settings.BorderMinZ, settings.BorderMaxZ);
                cameraComponent.camera.position = new Vector3(posX, cameraComponent.camera.position.y, posZ);
            }

            if (inputComponent.isEndDrag)
                cameraComponent.lastCameraPosition = cameraComponent.camera.position;
        }
    }
}
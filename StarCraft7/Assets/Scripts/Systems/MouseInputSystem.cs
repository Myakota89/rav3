﻿using System;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class MouseInputSystem : IEcsRunSystem
    {
        private EcsFilter<InputComponent> inputFilter = default;
        
        public void Run()
        {
            ref var inputComponent = ref inputFilter.Get1(0);
            inputComponent.inputPosition = Input.mousePosition;

            if (Input.GetMouseButtonDown(0))
            {
                inputComponent.isClick = true;
                inputComponent.clickPosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
                inputComponent.isDrag = true;
            else if (Input.GetMouseButtonUp(0))
                inputComponent.isEndDrag = true;
            else if (Input.GetMouseButtonDown(1))
                inputComponent.isCancel = true;
            else
            {
                inputComponent.isClick = false;
                inputComponent.isDrag = false;
                inputComponent.isEndDrag = false;
                inputComponent.isCancel = false;
                
            }
        }
    }
}
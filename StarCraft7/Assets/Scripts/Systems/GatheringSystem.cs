﻿using System;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class GatheringSystem : IEcsRunSystem
    {
        private readonly BuildingsCollection buildingsCollection = default;
        private readonly GameSettings gameSettings = default;
        private readonly EcsFilter<InputComponent> inputEntity = default;
        private readonly EcsFilter<GameStateComponent, GameResourcesComponent> sessionEntity = default;
        private readonly EcsFilter<MiningComponent, ColliderComponent, TimeComponent> miningEntities = default;
        
        public void Run()
        {
            if (CanRayCast())
            {
                var input = inputEntity.Get1(0);
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(input.inputPosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, gameSettings.BuildingMask))
                {
                    foreach (var i in miningEntities)
                    {
                        if (miningEntities.Get2(i).collider == hit.collider)
                            TryGatheringResources(i);
                    }
                }
            }
        }

        private bool CanRayCast() =>
            inputEntity.Get1(0).isClick && sessionEntity.Get1(0).state != GameState.Constructing;

        private void TryGatheringResources(int index)
        {
            if (!miningEntities.Get1(index).isFilled)
                return;
            
            ref var miningComponent = ref miningEntities.Get1(index);
            ref var timeComponent = ref miningEntities.Get3(index);
            ref var resources = ref sessionEntity.Get2(0);
            
            switch (miningComponent.minerType)
            {
                case MinerType.GasMiner:
                    if (resources.gasFilled)
                        return;
                    resources.gas += miningComponent.miningValue;
                    break;
                
                case MinerType.MineralsMiner:
                    if (resources.mineralsFilled)
                        return;
                    resources.minerals += miningComponent.miningValue;
                    break;
            }

            miningComponent.isFilled = false;
            timeComponent.miningTimer = timeComponent.miningTime;

        }

        private bool CanGathering(float maxValue, float value) =>
            value < maxValue;



    }
    
    
}
﻿using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class MiningSystem : IEcsRunSystem
    {
        private readonly EcsFilter<MiningComponent, BuildingComponent, BuildingViewComponent, TimeComponent> miningEntities = default;
        
        public void Run()
        {
            foreach (var i in miningEntities)
            {
                if (miningEntities.Get2(i).state != BuildingState.Mining)
                    continue;
                
                ref var miningComponent = ref miningEntities.Get1(i);
                ref var viewComponent = ref miningEntities.Get3(i);
                ref var timeComponent = ref miningEntities.Get4(i);

                if (timeComponent.miningTimer.TotalSeconds < 1)
                {
                    miningComponent.isFilled = true;
                    viewComponent.view.SetTimerText("Full");
                }
                    
                else
                {
                    viewComponent.view.SetTimerText(timeComponent.miningTimer.ToString());
                    viewComponent.view.SetTimerColor(Color.green);
                }
            }
        }
    }
}
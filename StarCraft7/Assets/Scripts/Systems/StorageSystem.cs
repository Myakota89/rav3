﻿using Components;
using Leopotam.Ecs;
using ScriptableObjects;

namespace Systems
{
    public class StorageSystem : IEcsRunSystem
    {
        private GameSettings gameSettings = default;
        private readonly EcsFilter<GameResourcesComponent> sessionEntity = default;
        private readonly EcsFilter<StorageComponent, BuildingComponent, BuildingViewComponent> storageEntitys = default;
        
        public void Run()
        {
            int gasSum = default;
            int mineralsSum = default;
            foreach (var i in storageEntitys)
            {
                if (storageEntitys.Get2(i).state != BuildingState.Built)
                    continue;

                var storageComponent = storageEntitys.Get1(i);
                var viewComponent = storageEntitys.Get3(i);
                viewComponent.view.SetTimerText("+" + storageComponent.storageValue);

                switch (storageComponent.storageType)
                {
                    case StorageType.GasStorage:
                        gasSum += storageComponent.storageValue;
                        break;
                    case StorageType.MineralsStorage:
                        mineralsSum += storageComponent.storageValue;
                        break;
                }
            }
            
            ref var resources = ref sessionEntity.Get1(0);
            resources.maxAmountGas = gasSum + gameSettings.StartMaxAmountGas;
            resources.maxAmountMinerals = mineralsSum + gameSettings.StartMaxAmountMinerals;
        }
    }
}
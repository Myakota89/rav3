﻿using Components;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using UnityEngine;
using UnityEngine.UI;

namespace Systems
{
    public class ScreenUiSystem : IEcsRunSystem
    {
        private EcsFilter<GameResourcesComponent> resourcesFilter = default;
        [EcsUiNamed("GasAmountText")] Text gasTextUI;
        [EcsUiNamed("MineralsAmountText")] Text mineralsTextUI;

        
        public void Run()
        {
            var resources = resourcesFilter.Get1(0);
            gasTextUI.text = "Gas: " + resources.gas + "/" + resources.maxAmountGas;
            mineralsTextUI.text = "Minerals: " + resources.minerals + "/" + resources.maxAmountMinerals;
        }

        
    }
}
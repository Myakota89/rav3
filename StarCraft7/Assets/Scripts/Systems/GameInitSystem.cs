﻿using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class GameInitSystem : IEcsInitSystem
    {

        private GameSettings gameSettings = default;
        private EcsWorld world = default;
        
        public void Init()
        {
            var cameraEntity = world.NewEntity();
            cameraEntity.Get<CameraComponent>().camera = Camera.main.transform;

            var inputEntity = world.NewEntity();
            inputEntity.Get<InputComponent>();

            var timerEntity = world.NewEntity();
            timerEntity.Get<TimeComponent>();

            var sessionEntity = world.NewEntity();
            sessionEntity.Get<GameStateComponent>();
            sessionEntity.Get<GameResourcesComponent>().gas = gameSettings.StartAmountGas;
            sessionEntity.Get<GameResourcesComponent>().minerals = gameSettings.StartAmountMinerals;
            sessionEntity.Get<GameResourcesComponent>().maxAmountGas = gameSettings.StartMaxAmountGas;
            sessionEntity.Get<GameResourcesComponent>().maxAmountMinerals = gameSettings.StartMaxAmountMinerals;
        }
    }
}
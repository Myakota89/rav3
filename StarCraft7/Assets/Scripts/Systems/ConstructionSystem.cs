﻿using System;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class ConstructionSystem : IEcsRunSystem
    {
        private EcsWorld world = default;
        private GameSettings gameSettings = default;
        private BuildingsCollection buildingsCollection = default;

        private EcsFilter<BuildingComponent, BuildingViewComponent, TimeComponent, ColliderComponent> buildingEntities = default;
        private EcsFilter<InputComponent> inputFilter = default;
        private EcsFilter<GameStateComponent, GameResourcesComponent> sessionFilter = default;

        public void Run()
        {
            foreach (var i in buildingEntities)
            {

                if (buildingEntities.Get1(i).state == BuildingState.ShopSelection)
                {
                    ref var buildingView = ref buildingEntities.Get2(i);
                    ref var buildingComponent = ref buildingEntities.Get1(i);
                    ref var buildingCollider = ref buildingEntities.Get4(i);
                    var buildingSettings = GetBuildingSettings(i);
                    var buildingGameObject = GameObject.Instantiate(buildingSettings.PrefabBuilding);
                    
                    buildingView.gameObject = buildingGameObject;
                    buildingView.view = buildingGameObject.GetComponent<BuildingView>();
                    buildingComponent.state = BuildingState.Created;
                    buildingCollider.collider = buildingGameObject.GetComponent<Collider>();
                    buildingCollider.colliderEmitter = buildingGameObject.GetComponent<ColliderEmitter>();
                }
                
                if (buildingEntities.Get1(i).state == BuildingState.Created)
                {
                    ref var input = ref inputFilter.Get1(0);
                    ref var buildingComponent = ref buildingEntities.Get1(i);
                    ref var buildingView = ref buildingEntities.Get2(i);
                    ref var buildingTime = ref buildingEntities.Get3(i);
                    ref var buildingCollider = ref buildingEntities.Get4(i);
                    
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(input.inputPosition);
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, gameSettings.GroundMask))
                    {
                        var heightOffset = buildingView.gameObject.transform.localScale.y / 2;
                        buildingView.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y + heightOffset, hit.point.z);
                    }

                    if (buildingCollider.colliderEmitter.OnCollision)
                        buildingView.view.SetMaterialColor(Color.red);
                    else
                        buildingView.view.SetMaterialColor(Color.white);
                    

                    if (input.isClick)
                    {
                        if (buildingCollider.colliderEmitter.OnCollision)
                            return;
                        
                        var buildingSettings = GetBuildingSettings(i);
                        ref var resources = ref sessionFilter.Get2(0);

                        if (CanBuild(buildingSettings, resources))
                        {
                            sessionFilter.Get1(0).state = GameState.Build;
                            resources.gas -= buildingSettings.BuildGasCost;
                            resources.minerals -= buildingSettings.BuildMineralsCost;
                            buildingTime.constructingTimer = buildingSettings.BuildTime;
                            buildingView.view.SetMaterial(buildingSettings.Material);
                            buildingComponent.state = BuildingState.Constructing;
                            buildingComponent.level = buildingSettings.BuildingLevel;
                        }
                    }

                    if (input.isCancel)
                    {
                        GameObject.Destroy(buildingView.gameObject);
                        buildingEntities.GetEntity(i).Destroy();
                        sessionFilter.Get1(0).state = GameState.Free;
                    }
                }
            }
        }

        private bool CanBuild(BuildingSettings buildSettings, GameResourcesComponent resources) =>
            buildSettings.BuildGasCost <= resources.gas && buildSettings.BuildMineralsCost <= resources.minerals;

        private BuildingSettings GetBuildingSettings(int index)
        {
            var buildingComponent = buildingEntities.Get1(index);
            ref var buildingTime = ref buildingEntities.Get3(index);

            switch (buildingComponent.type)
            {
                case BuildingType.Miner:
                    var settings = buildingsCollection.GetMinerSettingsByName(buildingComponent.name);
                    ref var miningComponent = ref buildingEntities.GetEntity(index).Get<MiningComponent>();
                    miningComponent.minerType = settings.MinerType;
                    miningComponent.miningValue = settings.MiningValue;
                    buildingTime.miningTime = settings.MiningTime;
                    return settings;
                
                case BuildingType.Storage:
                    var storageSettings = buildingsCollection.GetStorageSettingsByName(buildingComponent.name);
                    ref var storageComponent = ref buildingEntities.GetEntity(index).Get<StorageComponent>();
                    storageComponent.storageType = storageSettings.StorageType;
                    storageComponent.storageValue = storageSettings.StorageValue;
                    return storageSettings;
            }

            throw new Exception($"{buildingComponent.type} - incorrect type");
        }

    }
}
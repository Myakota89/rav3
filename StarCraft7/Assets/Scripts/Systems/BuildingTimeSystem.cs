﻿using System;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class BuildingTimeSystem : IEcsRunSystem
    {
        private readonly EcsFilter<BuildingComponent, TimeComponent> buildingEntities = default;
        private readonly EcsFilter<BuildingComponent, TimeComponent> miningEntities = default;
        
        public void Run()
        {
            foreach (var i in buildingEntities)
            {
                ref var buildingComponent = ref buildingEntities.Get1(i);
                ref var timeComponent = ref buildingEntities.Get2(i);
                timeComponent.gameTime += Time.deltaTime;
                
                if (timeComponent.gameTime > 1)
                {
                    timeComponent.gameTime = 0;
                    if (buildingComponent.state == BuildingState.Constructing)
                        timeComponent.constructingTimer = timeComponent.constructingTimer.Add(new TimeSpan(0,0,-1));

                    var entity = buildingEntities.GetEntity(i);

                    if (entity.Has<MiningComponent>())
                        if (buildingComponent.state == BuildingState.Mining && !entity.Get<MiningComponent>().isFilled)
                            timeComponent.miningTimer = timeComponent.miningTimer.Add(new TimeSpan(0,0,-1));

                }
            }
        }
    }
}
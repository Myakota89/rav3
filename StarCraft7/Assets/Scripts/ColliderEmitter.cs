﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ColliderEmitter : MonoBehaviour
{
    public bool OnCollision = default;
    public HashSet<Collider> colliders = new HashSet<Collider>();

    private void Update()
    {
        if (colliders.Count > 0)
            OnCollision = true;
        else
            OnCollision = false;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            //OnCollision = true;
            colliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            //OnCollision = false;
            colliders.Remove(other);
        }
        
    }
}
﻿using TMPro;
using UnityEngine;

public class BuildingView : MonoBehaviour
{
    [SerializeField] private TextMeshPro textTimer = default;

    public void SetMaterialColor(Color color) =>
        GetComponent<Renderer>().material.color = color;

    public void SetMaterial(Material material) =>
        GetComponent<Renderer>().material = material;

    public void SetTimerText(string text) =>
        textTimer.text = text;

    public void SetTimerColor(Color color) =>
        textTimer.color = color;
}
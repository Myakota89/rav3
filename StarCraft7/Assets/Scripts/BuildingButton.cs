﻿using UnityEngine;

public class BuildingButton : MonoBehaviour
{

    [SerializeField] private BuildingType buildyngType = default;
    [SerializeField] private string buildingName = default;

    public BuildingType BuildingType => buildyngType;
    public string BuildingName => buildingName;
}
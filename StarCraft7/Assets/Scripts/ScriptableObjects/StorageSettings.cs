﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Storage")]
    public class StorageSettings : BuildingSettings
    {
        [SerializeField] private int storageValue = default;
        [SerializeField] private StorageType storageType = default;
        private BuildingType type = BuildingType.Storage;
        
        public int StorageValue => storageValue;
        public StorageType StorageType => storageType;
        public BuildingType Type => type;
    }
}
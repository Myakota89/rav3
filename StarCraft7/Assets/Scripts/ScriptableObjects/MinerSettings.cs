﻿
using System;
using TMPro;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Miner")]
    public class MinerSettings : BuildingSettings
    {
        [SerializeField] private int miningValue = default;
        [SerializeField] private MinerType minerType = default;
        [Header("Mining time")]
        [SerializeField] private int miningHours = default;
        [SerializeField] private int miningMinutes = default;
        [SerializeField] private int miningSeconds = default;
        private BuildingType type = BuildingType.Miner;

        public int MiningValue => miningValue;
        public MinerType MinerType => minerType;
        public BuildingType Type => type;
        public TimeSpan MiningTime => new TimeSpan(miningHours, miningMinutes, miningSeconds);

        private void OnValidate()
        {
            miningHours = miningHours < 0 ? 0 : miningHours;
            miningHours = miningHours > 59 ? 59 : miningHours;
            miningMinutes = miningMinutes < 0 ? 0 : miningMinutes;
            miningMinutes = miningMinutes > 59 ? 59 : miningMinutes;
            miningSeconds = miningSeconds < 0 ? 0 : miningSeconds;
            miningSeconds = miningSeconds > 59 ? 59 : miningSeconds;
        }
    }
}
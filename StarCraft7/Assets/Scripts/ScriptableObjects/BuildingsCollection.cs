﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Building Collection")]
    public class BuildingsCollection : ScriptableObject
    {
        [SerializeField] private List<MinerSettings> minerBuildingSettings = default;
        [SerializeField] private List<StorageSettings> storageBuildingSettings = default;

        public MinerSettings GetMinerSettingsByName(string buildingName)
        {
            var settings = minerBuildingSettings.Find(s => s.BuildingName == buildingName);

            if (!settings)
                throw new Exception($"Building {buildingName} not found in Building Collection(MinerSettings)");

            return settings;
        }

        public StorageSettings GetStorageSettingsByName(string buildingName)
        {
            var settings = storageBuildingSettings.Find(s => s.BuildingName == buildingName);

            if (!settings)
                throw new Exception($"Building {buildingName} not found if Building Collection(StorageSettings)");

            return settings;
        }
    }
}
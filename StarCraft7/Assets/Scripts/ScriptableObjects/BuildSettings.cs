﻿using System;
using UnityEngine;

namespace ScriptableObjects
{
    public class BuildSettings : ScriptableObject
    {
        [SerializeField] private int buildGasCost = default;
        [SerializeField] private int buildMineralsCost = default;
        [Header("Build time")]
        [SerializeField] private int buildHours = default;
        [SerializeField] private int buildMinutes = default;
        [SerializeField] private int buildSeconds = default;

        public int BuildGasCost => buildGasCost;
        public int BuildMineralsCost => buildMineralsCost;
        public TimeSpan BuildTime => new TimeSpan(buildHours, buildMinutes, buildSeconds);

        private void OnValidate()
        {
            buildGasCost = buildGasCost < 0 ? 0 : buildGasCost;
            buildMineralsCost = buildMineralsCost < 0 ? 0 : buildMineralsCost;
            
            buildHours = buildHours < 0 ? 0 : buildHours;
            buildHours = buildHours > 59 ? 59 : buildHours;
            buildMinutes = buildMinutes < 0 ? 0 : buildMinutes;
            buildMinutes = buildMinutes > 59 ? 59 : buildMinutes;
            buildSeconds = buildSeconds < 0 ? 0 : buildSeconds;
            buildSeconds = buildSeconds > 59 ? 59 : buildSeconds;
        }
    }
}
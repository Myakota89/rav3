﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Camera Settings")]
    public class CameraSettings : ScriptableObject
    {
        [SerializeField] private float borderMinX = default;
        [SerializeField] private float borderMaxX = default;
        [SerializeField] private float borderMinZ = default;
        [SerializeField] private float borderMaxZ = default;
        [SerializeField] private float cameraMoveSpeed = default;

        public float BorderMaxX => borderMaxX;
        public float BorderMinX => borderMinX;
        public float BorderMaxZ => borderMaxZ;
        public float BorderMinZ => borderMinZ;
        public float CameraMoveSpeed => cameraMoveSpeed;
    }
}
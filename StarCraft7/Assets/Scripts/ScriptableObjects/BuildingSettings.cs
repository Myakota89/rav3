﻿using UnityEngine;

namespace ScriptableObjects
{
    public class BuildingSettings : BuildSettings
    {
        [SerializeField] private string buildingName = default;
        [SerializeField] private GameObject prefabBuilding = default;
        [SerializeField] private Material material = default;
        [SerializeField] private int buildingLevel = default;
        
        public string BuildingName => buildingName;
        public GameObject PrefabBuilding => prefabBuilding;
        public int BuildingLevel => buildingLevel;
        public Material Material => material;
        
        private void OnValidate()
        {
            buildingLevel = buildingLevel < 0 ? 0 : buildingLevel;
        }
    }
}
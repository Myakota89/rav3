﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeField] private LayerMask groundMask = default;
        [SerializeField] private LayerMask buildingMask = default;
        [SerializeField] private int startAmountMinerals = default;
        [SerializeField] private int startAmountGas = default;
        [SerializeField] private int startMaxAmountMinerals = default;
        [SerializeField] private int startMaxAmountGas = default;

        public LayerMask GroundMask => groundMask;
        public LayerMask BuildingMask => buildingMask;
        public int StartAmountGas => startAmountGas;
        public int StartAmountMinerals => startAmountMinerals;
        public int StartMaxAmountGas => startMaxAmountGas;
        public int StartMaxAmountMinerals => startMaxAmountMinerals;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using Systems;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using ScriptableObjects;
using UnityEngine;

public class ECSInitializer : MonoBehaviour
{

    [SerializeField] private EcsUiEmitter uiEmitter = default;
    [SerializeField] private CameraSettings cameraSettings = default;
    [SerializeField] private GameSettings gameSettings = default;
    [SerializeField] private BuildingsCollection buildingsCollection = default;

    private EcsWorld world = default;
    private EcsSystems systems = default;

    private void Start()
    {
        world = new EcsWorld();
        systems = new EcsSystems(world);
        
        systems.Add(new GameInitSystem())
            .Add(new MouseInputSystem())
            .Add(new CameraMoveSystem())
            .Add(new ShopUiSystem())
            .Add(new ScreenUiSystem())
            .Add(new ConstructionSystem())
            .Add(new BuildingTimeSystem())
            .Add(new BuildSystem())
            .Add(new MiningSystem())
            .Add(new GatheringSystem())
            .Add(new StorageSystem())
            .Add(new ResourceSystem())
            .Inject(cameraSettings)
            .Inject(gameSettings)
            .Inject(buildingsCollection)
            .InjectUi(uiEmitter)
            .Init();
    }

    private void Update()
    {
        systems.Run();
    }

    private void OnDestroy()
    {
        world.Destroy();
        systems.Destroy();
    }
}

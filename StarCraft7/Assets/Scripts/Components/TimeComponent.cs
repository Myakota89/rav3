﻿using System;

namespace Components
{
    public struct TimeComponent
    {
        public float gameTime;
        public TimeSpan miningTime;
        public TimeSpan miningTimer;
        public TimeSpan constructingTimer;
        
    }
}
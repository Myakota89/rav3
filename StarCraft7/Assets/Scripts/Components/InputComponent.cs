﻿using UnityEngine;

namespace Components
{
    public struct InputComponent
    {
        public Vector2 inputPosition;
        public Vector2 clickPosition;
        public bool isClick;
        public bool isDrag;
        public bool isEndDrag;
        public bool isCancel;
    }
}
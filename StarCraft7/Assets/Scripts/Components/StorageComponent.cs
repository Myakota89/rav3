﻿namespace Components
{
    public struct StorageComponent
    {
        public StorageType storageType;
        public int storageValue;
    }
}
﻿namespace Components
{
    public struct GameResourcesComponent
    {
        public int minerals;
        public int gas;
        public int maxAmountMinerals;
        public int maxAmountGas;
        public bool gasFilled;
        public bool mineralsFilled;
    }
}
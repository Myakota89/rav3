﻿using UnityEngine;

namespace Components
{
    public struct CameraComponent
    {
        public Transform camera;
        public Vector3 lastCameraPosition;
    }
}
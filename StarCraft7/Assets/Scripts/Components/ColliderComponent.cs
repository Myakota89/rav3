﻿using UnityEngine;

namespace Components
{
    public struct ColliderComponent
    {
        public Collider collider;
        public ColliderEmitter colliderEmitter;
    }
}
﻿using System;
using UnityEngine;

namespace Components
{
    public struct BuildingComponent
    {
        public string name;
        public BuildingState state;
        public BuildingType type;
        public int level;
    }
}
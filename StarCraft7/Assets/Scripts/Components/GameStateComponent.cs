﻿using DefaultNamespace;

namespace Components
{
    public struct GameStateComponent
    {
        public GameState state;
    }
}
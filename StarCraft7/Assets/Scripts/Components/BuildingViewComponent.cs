﻿using UnityEngine;

namespace Components
{
    public struct BuildingViewComponent
    {
        public GameObject gameObject;
        public BuildingView view;
    }
}
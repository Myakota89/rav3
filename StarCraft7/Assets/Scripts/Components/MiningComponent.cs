﻿namespace Components
{
    public struct MiningComponent
    {
        public bool isFilled;
        public MinerType minerType;
        public int miningValue;
    }
}
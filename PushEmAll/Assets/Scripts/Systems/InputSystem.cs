﻿using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class InputSystem : IEcsRunSystem
    {
        private Joystick joystick = default;
        private EcsFilter<InputComponent> inputComponent = default;
        
        public void Run()
        {
            inputComponent.Get1(0).direction = joystick.direction;
        }
    }
}
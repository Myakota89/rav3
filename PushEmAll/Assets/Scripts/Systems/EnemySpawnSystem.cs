﻿using System.Collections.Generic;
using System.Linq;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.AI;

namespace Systems
{
    public class EnemySpawnSystem : IEcsInitSystem
    {
        private EcsWorld world = default;
        private PrefabsCollection prefabs = default;
        private UnitSettings settings = default;
        private EcsFilter<SpawnAreaComponent> areas = default;

        public void Init()
        {
            var enemys = new List<Transform>();

            foreach (var i in areas)
            {
                var area = areas.Get1(i);

                var counter = 0;
                while (counter < 5)
                {
                    var rndX = Random.Range(area.minPoint.x + 0.1f, area.maxPoint.x - 0.1f);
                    var rndZ = Random.Range(area.minPoint.y + 0.1f, area.maxPoint.y - 0.1f);
                    var enemy = GameObject.Instantiate(prefabs.EnemyPrefab, new Vector3(rndX, 12.2f, rndZ), Quaternion.identity);
                    var closestEnemy = enemys.FirstOrDefault(e => Vector3.Distance(enemy.transform.position, e.position) < 0.1f);
                    if (closestEnemy == default)
                    {
                        counter++;
                        enemys.Add(enemy.transform);
                        var enemyEntity = world.NewEntity();
                        enemyEntity.Get<FollowComponent>().navMeshAgent = enemy.GetComponent<NavMeshAgent>();
                        enemyEntity.Get<MoveableComponent>().animator = enemy.GetComponent<Animator>();
                        enemyEntity.Get<MoveableComponent>().transform = enemy.transform;
                        enemyEntity.Get<ColliderComponent>().transform = enemy.transform;
                        enemyEntity.Get<ColliderComponent>().contactCollisions = enemy.GetComponent<ColliderEmitter>().contactCollision;
                    }
                    else
                        GameObject.DestroyImmediate(enemy);

                }
            }
        } 
    }

}
﻿using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class FollowSystem : IEcsRunSystem
    {
        EcsFilter<FollowComponent, MoveableComponent> filter = default;
        EcsFilter<PlayerComponent, MoveableComponent> player = default;

        public void Run()
        {
            foreach (var i in filter)
            {
                filter.Get1(i).navMeshAgent.destination = player.Get1(0).rigidBody.transform.position;
                filter.Get1(i).navMeshAgent.updateRotation = false;
                filter.Get2(i).animator.SetBool("Move", filter.Get1(i).navMeshAgent.velocity != Vector3.zero);
                ref var playerComponent = ref player.Get1(0);
                var lookDirection = playerComponent.rigidBody.transform.position - filter.Get2(i).transform.position;
                filter.Get2(i).transform.rotation = Quaternion.LookRotation(lookDirection);
            }
            
        }
    }
}
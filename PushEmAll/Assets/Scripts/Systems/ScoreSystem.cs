﻿using Components;
using Leopotam.Ecs;
using ScriptableObjects;

namespace Systems
{
    public class ScoreSystem : IEcsRunSystem
    {
        private GameSettings settings = default;
        private EcsFilter<ScoreComponent> filter = default;
        
        public void Run()
        {
            ref var enemyAmount = ref filter.Get1(0).enemyAmount;
            ref var crystalAmount = ref filter.Get1(0).crystalAmount;

            filter.Get1(0).score = (enemyAmount * settings.EnemyScore) + (crystalAmount * settings.CrystalScore);
        }
    }
}
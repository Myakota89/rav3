﻿using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class GameInitSystem : IEcsInitSystem
    {
        private EcsWorld world = default;
        private PrefabsCollection prefabs = default;
        private GameSettings gameSettings = default;
        private CharacterController player = default;
        private Animator weaponAnimator = default;
        private List<BoxCollider> areas = default;

        public void Init()
        {

            var playerEntity = world.NewEntity();
            playerEntity.Get<PlayerComponent>().characterController = player;
            playerEntity.Get<PlayerComponent>().rigidBody = player.GetComponent<Rigidbody>();
            playerEntity.Get<MoveableComponent>().transform = player.gameObject.transform;
            playerEntity.Get<MoveableComponent>().animator = player.GetComponent<Animator>();
            playerEntity.Get<WeaponComponent>().animator = weaponAnimator;
            playerEntity.Get<InputComponent>();

            foreach (var area in areas)
            {
                var spawnAreaEntity = world.NewEntity();
                spawnAreaEntity.Get<SpawnAreaComponent>().minPoint = new Vector2(area.bounds.min.x, area.bounds.min.z);
                spawnAreaEntity.Get<SpawnAreaComponent>().maxPoint = new Vector2(area.bounds.max.x, area.bounds.max.z);
            }
        }

    }
}

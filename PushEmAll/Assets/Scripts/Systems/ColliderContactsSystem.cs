﻿using System;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class ColliderContactsSystem : IEcsRunSystem
    {
        private EcsFilter<ColliderComponent> filter = default;
        private EcsFilter<PlayerComponent, ScoreComponent> player = default;
        
        public void Run()
        {
            var playerTag = player.Get1(0).characterController.tag;
            
            foreach (var i in filter)
            {
                foreach (var collision in filter.Get1(i).contactCollisions)
                {
                    if (collision.gameObject.CompareTag(playerTag))
                    {
                        if (filter.Get1(i).transform.CompareTag("Crystal"))
                        {
                            player.Get2(0).crystalAmount++;
                            GameObject.DestroyImmediate(filter.Get1(i).transform.gameObject);
                            filter.GetEntity(i).Destroy();
                        }
                    }

                }
                
                
            }
        }
    }
}
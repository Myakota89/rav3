﻿using System.Collections.Generic;
using System.Linq;
using Components;
using DefaultNamespace;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class CrystalSpawnSystem : IEcsInitSystem
    {
        private EcsWorld world = default;
        private PrefabsCollection prefabs = default;
        private GameSettings settings = default;
        private EcsFilter<SpawnAreaComponent> areas = default;

        public void Init()
        {
            var crystals = new List<Transform>();

            foreach (var i in areas)
            {
                var area = areas.Get1(i);

                var counter = 0;
                while (counter < 3)
                {
                    var rndX = Random.Range(area.minPoint.x + 0.1f, area.maxPoint.x - 0.1f);
                    var rndZ = Random.Range(area.minPoint.y + 0.1f, area.maxPoint.y - 0.1f);
                    var crystal = GameObject.Instantiate(prefabs.CrystalPrefab, new Vector3(rndX, 12.2f, rndZ), Quaternion.Euler(-90,0,0));
                    var closestCrystal = crystals.FirstOrDefault(c => Vector3.Distance(crystal.transform.position, c.position) < settings.MinCrystalSpawnDistance);
                    if (closestCrystal == default)
                    {
                        counter++;
                        crystals.Add(crystal.transform);

                        var crystalEntity = world.NewEntity();
                        crystalEntity.Get<ColliderComponent>().contactCollisions =
                            crystal.GetComponent<ColliderEmitter>().contactCollision;
                        crystalEntity.Get<ColliderComponent>().transform = crystal.transform;
                    }
                    else
                        GameObject.DestroyImmediate(crystal);

                }
            }
        } 
    }
}
﻿using System;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class PlayerMoveSystem : IEcsRunSystem
    {
	    private UnitSettings unitSettings = default;
        private EcsFilter<PlayerComponent, MoveableComponent, InputComponent> playerMoveFilter = default;
        private EcsFilter<WeaponComponent> weapon = default;
        
        public void Run()
        {
	        ref var direction = ref playerMoveFilter.Get3(0).direction;
	        ref var transform = ref playerMoveFilter.Get2(0).transform;
	        ref var characterController = ref playerMoveFilter.Get1(0).characterController;
	        ref var animator = ref playerMoveFilter.Get2(0).animator;

	        var moveDirection = Vector3.zero;
	        
	        if (characterController.isGrounded)
	        {
		        moveDirection = new Vector3(direction.x, 0, direction.y);
		        Quaternion targetRotation = moveDirection != Vector3.zero ? Quaternion.LookRotation(moveDirection) : transform.rotation;
		        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 400 * Time.deltaTime);
		        moveDirection *= unitSettings.Speed;
		        playerMoveFilter.Get3(0).lastDirection = direction;
	        }
	        else
	        {
		        ref var lastDirection = ref playerMoveFilter.Get3(0).lastDirection;
		        lastDirection.x = Mathf.Lerp(lastDirection.x, 0, Time.deltaTime / 0.5f);
		        lastDirection.y = Mathf.Lerp(lastDirection.y, 0, Time.deltaTime * 0.5f);
		        
		        moveDirection = new Vector3(lastDirection.x, 0, lastDirection.y);
	        }
	        
            moveDirection.y -= 300 * Time.deltaTime;
            characterController.Move(moveDirection * Time.deltaTime);
            animator.SetBool("Move", direction != Vector2.zero);
            weapon.Get1(0).animator.SetBool("Shoot", direction == Vector2.zero);

        }
    }
}
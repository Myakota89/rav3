﻿using UnityEngine;

namespace Components
{
    public struct UnitComponent
    {
        public float speed;
        public float superPunchPower;
    }
}
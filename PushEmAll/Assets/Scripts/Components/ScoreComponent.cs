﻿namespace Components
{
    public struct ScoreComponent
    {
        public int crystalAmount;
        public int enemyAmount;
        public int score;
    }
}
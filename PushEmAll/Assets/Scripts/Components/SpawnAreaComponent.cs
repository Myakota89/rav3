﻿using UnityEngine;

namespace Components
{
    public struct SpawnAreaComponent
    {
        public Vector2 minPoint;
        public Vector2 maxPoint;
    }
}
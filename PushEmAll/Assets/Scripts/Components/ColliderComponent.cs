﻿using System.Collections.Generic;
using UnityEngine;

namespace Components
{
    public struct ColliderComponent
    {
        public HashSet<Collider> contactCollisions;
        public Transform transform;
    }
}
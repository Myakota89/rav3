﻿using UnityEngine;
using UnityEngine.AI;

namespace Components
{
    public struct FollowComponent
    {
        public NavMeshAgent navMeshAgent;
        public Transform followTarget;
    }
}
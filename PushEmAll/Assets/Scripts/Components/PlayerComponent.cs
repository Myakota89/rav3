﻿using UnityEngine;

namespace Components
{
    public struct PlayerComponent
    {
        public CharacterController characterController;
        public Rigidbody rigidBody;
    }
}
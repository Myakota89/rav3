﻿using UnityEngine;

namespace Components
{
    public struct InputComponent
    {
        public Vector2 direction;
        public Vector2 lastDirection;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	
   public Joystick joystick;
   public CharacterController controller;
   public Animator anim;
   public Image bulletIndicator;
   public Transform targetIndicator;
   public Transform target;
   public ParticleSystem movementEffect;
   public ParticleSystem shootingEffect;
   public GameObject bloodEffect;
   
   public Transform gunFront;
   public GameObject bullet;
   
   public float speed;
   public float gravity;
   public float bulletCount;
   public float reloadSpeed;
   
   Vector3 moveDirection;
   float bullets = 1f;
   
   bool reloading;
   
   List<GameObject> bulletStorage = new List<GameObject>();
   
   [HideInInspector]
   public bool safe;

   void Update()
   {

	   Vector2 direction = joystick.direction;
	   
	   if(controller.isGrounded)
	   {
            moveDirection = new Vector3(direction.x, 0, direction.y);
			
			Quaternion targetRotation = moveDirection != Vector3.zero ? Quaternion.LookRotation(moveDirection) : transform.rotation;
			transform.rotation = targetRotation;
	
            moveDirection = moveDirection * speed;
            
	   }
	   
       moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
       controller.Move(moveDirection * Time.deltaTime);
		
		
		
		if(anim.GetBool("Moving") != (direction != Vector2.zero))
			anim.SetBool("Moving", direction != Vector2.zero);

   }

}

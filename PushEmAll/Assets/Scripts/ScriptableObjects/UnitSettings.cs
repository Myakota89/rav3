﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Unit Settings")]
    public class UnitSettings : ScriptableObject
    {
        [SerializeField] private float speed = default;
        [SerializeField] private float superPunchPower = default;

        public float Speed => speed;
        public float SuperPunchPower => superPunchPower;
    }
}
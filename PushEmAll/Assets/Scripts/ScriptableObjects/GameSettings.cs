﻿using System;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/GameSettings")]
    public class GameSettings : ScriptableObject
    {

        [SerializeField] private int enemyScore = default;
        [SerializeField] private int crystalScore = default;
        [SerializeField][Range(0.1f, 1)] private float minCrystalSpawnDistance = default;

        public float MinCrystalSpawnDistance => minCrystalSpawnDistance;
        public int EnemyScore => enemyScore;
        public int CrystalScore => crystalScore;

        private void OnValidate()
        {
            if (enemyScore < 0)
                enemyScore = 0;

            if (crystalScore < 0)
                crystalScore = 0;

        }
    }
}
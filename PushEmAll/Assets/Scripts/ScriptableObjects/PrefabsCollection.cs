﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Prefabs Collection")]
    public class PrefabsCollection : ScriptableObject
    {
        [SerializeField] private GameObject enemyPrefab = default;
        [SerializeField] private GameObject crystalPrefab = default;

        public GameObject CrystalPrefab => crystalPrefab;
        public GameObject EnemyPrefab => enemyPrefab;
        
        
    }
}
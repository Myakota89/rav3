﻿
using System;
using System.Collections.Generic;
using Systems;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

public class ECSInitializer : MonoBehaviour
{
    [SerializeField] private PrefabsCollection prefabs = default;
    [SerializeField] private UnitSettings unitSettings = default;
    [SerializeField] private GameSettings gameSettings = default;
    [SerializeField] private Joystick joystick = default;
    [SerializeField] private CharacterController characterController = default;
    [SerializeField] private Animator weaponAnimator = default;
    [SerializeField] private List<BoxCollider> areas = default;

    private EcsWorld world = default;
    private EcsSystems systems = default;

    private void Start()
    {
        world = new EcsWorld();
        systems = new EcsSystems(world);
        
        systems.Add(new InputSystem())
            .Add(new GameInitSystem())
            .Add(new PlayerMoveSystem())
            .Add(new EnemySpawnSystem())
            .Add(new CrystalSpawnSystem())
            .Add(new ColliderContactsSystem())
            .Add(new ScoreSystem())
            .Add(new FollowSystem())
            .Inject(unitSettings)
            .Inject(prefabs)
            .Inject(joystick)
            .Inject(gameSettings)
            .Inject(characterController)
            .Inject(weaponAnimator)
            .Inject(areas)
            .Init();
    }

    private void Update()
    {
        systems.Run();
    }

    private void OnDestroy()
    {
        world.Destroy();
        systems.Destroy();
    }
}

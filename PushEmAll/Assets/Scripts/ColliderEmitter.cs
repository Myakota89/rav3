﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DefaultNamespace
{
    public class ColliderEmitter : MonoBehaviour
    {
        public HashSet<Collider> contactCollision = new HashSet<Collider>();

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
                contactCollision.Add(other);
            else if (other.CompareTag("PlayerWeapon"))
                GetComponent<NavMeshAgent>().updatePosition = false;
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
                contactCollision.Remove(other);
            else if (other.CompareTag("PlayerWeapon"))
                GetComponent<NavMeshAgent>().updatePosition = true;
        }

    }
}
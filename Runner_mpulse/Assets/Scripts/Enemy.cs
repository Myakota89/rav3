﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IRessurection
{
    private bool isAttack = default;
    private Animator anim = default;
    public bool isDead = false;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        var hit = Physics2D.Raycast(transform.position, Vector2.left, 1.5f, LayerMask.GetMask("Player"));
        if (hit.collider?.GetComponent<Player>())
            Attack(hit.collider.GetComponent<Player>());
    }

    public void Attack(Player player)
    {
        if (isAttack || player.isDead)
            return;

        isAttack = true;
        anim.SetTrigger("Attack");
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return new WaitForSeconds(0.6f);
            if (!isDead)
            {
                player.Hit();
                isAttack = false;
            }
            
        }
        
    }

    public virtual void Hit()
    {
        if (isDead)
            return;
        GetComponent<BoxCollider2D>().enabled = false;
        anim = GetComponent<Animator>();
        anim.SetBool("Dead", true);
        isDead = true;
    }

    public void Resurection()
    {
        if (!isDead)
            return;

        if (anim == null)
            anim = GetComponent<Animator>();
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().color = Color.white;
        anim.SetBool("Dead", false);
        isDead = false;
        isAttack = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject CemetryImage = default;
    public GameObject SwampImage = default;
    public GameObject DomainImage = default;

    public GameObject CemetryButton = default;
    public GameObject SwampButton = default;
    public GameObject DomainButton = default;

    public List<Image> soundValues = default;
    public List<Image> musicValues = default;
    public Sprite activeValueSprite = default;
    public Sprite deactiveValueSprite = default;

    public GameObject musicPrefab = default;
    private GameObject music = default;

    private AudioSource audio = default;
    public AudioClip audioSelect = default;
    public AudioClip audioLose = default;
    public AudioClip audioWin = default;
    public AudioClip audioAttack = default;
    public AudioClip audioEnemyHit = default;
    public AudioClip audioPlayerHit = default;
    public AudioClip audioJump = default;
    public AudioClip audioPickup = default;
    public AudioClip audioDeath = default;
    public AudioClip audioSpin = default;
    public AudioClip audioCheckReward = default;

    public Setting settings = default;

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        Time.timeScale = 1.0f;
        audio = GetComponent<AudioSource>();
        music = GameObject.FindGameObjectWithTag("Music");
        if (music == null)
            music = Instantiate(musicPrefab);

        ChangeSoundVolume(settings.soundVolume);
        ChangeMusicVolume(settings.musicVolume);

    }

    public void SetState(int value)
    {
        CemetryImage.SetActive(false);
        SwampImage.SetActive(false);
        DomainImage.SetActive(false);

        CemetryButton.SetActive(false);
        SwampButton.SetActive(false);
        DomainButton.SetActive(false);

        if (value == 1)
        {
            CemetryButton.SetActive(true);
            CemetryImage.SetActive(true);
        }
        else if (value == 2)
        {
            SwampButton.SetActive(true);
            SwampImage.SetActive(true);
        }
        else if (value == 3)
        {
            DomainButton.SetActive(true);
            DomainImage.SetActive(true);
        }
    }

    public void StartLevel(int level)
    {
        if (level == 1)
            SceneManager.LoadScene("Cemetery");
        else if (level == 2)
            SceneManager.LoadScene("Swamp");
        else if (level == 3)
            SceneManager.LoadScene("Domain");

    }

    public void PlaySelect() =>
        audio.PlayOneShot(audioSelect);

    public void PlayLose() =>
        audio.PlayOneShot(audioLose);

    public void PlayWin() =>
        audio.PlayOneShot(audioWin);

    public void PlayAttack() =>
        audio.PlayOneShot(audioAttack);

    public void PlayEnemyHit() =>
        audio.PlayOneShot(audioEnemyHit);

    public void PlayPlayerHit() =>
        audio.PlayOneShot(audioPlayerHit);

    public void PlayJump() =>
        audio.PlayOneShot(audioJump);

    public void PlayPickup() =>
        audio.PlayOneShot(audioPickup);

    public void PlayDeath() =>
        audio.PlayOneShot(audioDeath);

    public void PlaySpin() =>
        audio.PlayOneShot(audioSpin);
    public void PlayCheckReward() =>
        audio.PlayOneShot(audioCheckReward);


    public void ChangeSoundVolume(float value)
    {
        ResetValues(soundValues);
        int activeValues = GetActiveValue(value);

        for (int i = 0; i < activeValues; i++)
            soundValues[i].sprite = activeValueSprite;

        GetComponent<AudioSource>().volume = value;
        settings.soundVolume = value;

    }

    public void ChangeMusicVolume(float value)
    {
        ResetValues(musicValues);
        int activeValues = GetActiveValue(value);

        for (int i = 0; i < activeValues; i++)
            musicValues[i].sprite = activeValueSprite;

        music.GetComponent<AudioSource>().volume = value;
        settings.musicVolume = value;
    }

    private void ResetValues(List<Image> values)
    {
        foreach (var val in values)
            val.sprite = deactiveValueSprite;
    }

    private int GetActiveValue(float values)
    {
        int result = 0;

        if (values > 0.9f)
            result = 10;
        else if (values > 0.8f)
            result = 9;
        else if (values > 0.7f)
            result = 8;
        else if (values > 0.6f)
            result = 7;
        else if (values > 0.5f)
            result = 6;
        else if (values > 0.4f)
            result = 5;
        else if (values > 0.3f)
            result = 4;
        else if (values > 0.2f)
            result = 3;
        else if (values > 0.1)
            result = 2;
        else if (values > 0)
            result = 1;
        else if (values == 0)
            result = 0;

        return result;
    }

}

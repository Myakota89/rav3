﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isJumped = default;
    public bool canAttack = true;
    public bool isDead = default;
    public float speed = default;
    public float jumpForce = default; 
    private float directionX = default;
    private Rigidbody2D rb = default;
    private Animator anim = default;
    private float rightLimit = -2; 
    public GameObject coinAnimation = default;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (isDead)
            return;

        Vector2 bottomLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        if (transform.position.x < bottomLeft.x)
            transform.position = new Vector2(bottomLeft.x, transform.position.y);

        transform.position += new Vector3(directionX * Time.deltaTime * speed * GameManager.Instance.rateSpeed, 0, 0);
        if (transform.position.x > rightLimit)
        {
            transform.position = new Vector2(rightLimit, transform.position.y);
            LevelMove();
        }
        else
            LevelStop();

        
    }

    public void SetRightLimit()
    {
        rightLimit = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)).x;
    }

    public void Move(Vector2 direction)
    {
        if (isDead)
            return;

        if (direction.y > 0.7f && CanJump() && !isJumped)
            Jump();
        directionX = direction.x;
        FlipSprite();
    }

    private void LevelMove() =>
        Runner2D.Instance.speed = 5;

    private void LevelStop() =>
        Runner2D.Instance.speed = 0;

    private void FlipSprite()
    {
        if (directionX < -0.1f)
            GetComponent<SpriteRenderer>().flipX = true;
        else if (directionX > 0.1f)
            GetComponent<SpriteRenderer>().flipX = false;      
    }
        

    public void Jump()
    {
        GameManager.Instance.PlayJump();
        rb.AddForce(Vector2.up * jumpForce * GameManager.Instance.rateJump, ForceMode2D.Impulse);
        isJumped = true;
        Invoke("ResetJumped", 1.5f);
    }

    private void ResetJumped() =>
        isJumped = false;

    private bool CanJump() =>
        Physics2D.Raycast(transform.position, Vector2.down, 1.1f, LayerMask.GetMask("Ground"));

    public void Attack()
    {
        if (!canAttack || isDead)
            return;

        GameManager.Instance.PlayAttack();
        canAttack = false;
        anim.SetTrigger("Attack");
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return new WaitForSeconds(0.2f);
            Vector2 direction = GetComponent<SpriteRenderer>().flipX ? Vector2.left : Vector2.right;
            var hit = Physics2D.CircleCast(transform.position, 2.0f, direction, 1.0f, LayerMask.GetMask("Enemy"));
            if (hit.collider?.GetComponent<Enemy>())
            {
                GameManager.Instance.PlayEnemyHit();
                hit.collider.GetComponent<Enemy>().Hit();
            }
            yield return new WaitForSeconds(1.0f);
            canAttack = true;
        }              
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin")
        {
            GameManager.Instance.PlayPickup();
            TakeCoin();
            collision.GetComponent<CircleCollider2D>().enabled = false;
            collision.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (collision.tag == "Booster")
        {
            GameManager.Instance.PlayPickup();
            TakeBooster();
            collision.GetComponent<CircleCollider2D>().enabled = false;
            collision.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (collision.tag == "Enemy")
        {
            Hit();
        }    

        else if (collision.tag == "FireBall")
        {
            Hit();
            Destroy(collision.gameObject);
        }
            
       
    }

    private void TakeCoin()
    {
        GameManager.Instance.AddCoin(1 * GameManager.Instance.rateCoins);
    }

    private void TakeBooster()
    {
        GameManager.Instance.BoosterGame();
    }

    public void Hit()
    {
        GameManager.Instance.PlayPlayerHit();
        GameManager.Instance.RemoveCoin(20);
        var anim = Instantiate(coinAnimation, transform.position, Quaternion.identity);
        Destroy(anim, 1);
        GameManager.Instance.rateCoins = 1;
        GameManager.Instance.rateJump = 1;
        GameManager.Instance.rateSpeed = 1;
    }

    public void Dead()
    {
        
        isDead = true;
        LevelStop();
        anim.SetTrigger("Dead");
    }

}

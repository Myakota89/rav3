﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Setting")]
public class Setting : ScriptableObject
{
    public float musicVolume = 0.0f;
    public float soundVolume = 0.0f;

}

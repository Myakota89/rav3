﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterGame : MonoBehaviour
{
    public bool isSpinned = default;
    private float speed = default;
    public GameObject wheel = default;
    public Text infoText = default;
    public GameObject infoMenu = default;
    public RewardType reward = default;
    private void Update()
    {
        if (isSpinned)
        {

            speed -= Time.deltaTime * 2;
            wheel.transform.Rotate(new Vector3(0, 0, speed));

            if (speed <= 0)
            {
                isSpinned = false;
                CheckReward();
            }
        }
    }

    private void CheckReward()
    {
        GameManager.Instance.PlayCheckReward();
        infoMenu.SetActive(true);
        switch (reward)
        {
            case RewardType.AddCoins:
                infoText.text = "You get 10 coins!";
                GameManager.Instance.AddCoin(10);
                break;
            case RewardType.DoubleCoins:
                infoText.text = "Now you will get double coins!";
                GameManager.Instance.rateCoins = 2;
                break;
            case RewardType.SuperJump:
                infoText.text = "You get Super Jump!";
                GameManager.Instance.rateJump = 1.2f;
                break;
            case RewardType.GodMode:
                infoText.text = "Enemys don't spawn 10 sec";
                GameManager.Instance.isGodMode = true;
                Invoke("GameManager.Instance.GodModeOff", 10.0f);
                break;
            case RewardType.SuperSpeed:
                infoText.text = "You get Super Speed";
                GameManager.Instance.rateSpeed = 1.5f;
                break;
            default:
                break;
        }

    }

    public void Spin()
    {
        if (isSpinned)
            return;

        GameManager.Instance.PlaySpin();
        speed = Random.Range(10, 15);
        isSpinned = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Reward>())
            reward = collision.GetComponent<Reward>().rewardType;

    }
}

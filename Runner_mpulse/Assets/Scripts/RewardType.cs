﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RewardType
{
    AddCoins,
    DoubleCoins,
    SuperJump,
    GodMode,
    SuperSpeed
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Enemy
{
    private Vector3 direction = Vector3.left;
    public float speed = default;

    private void Update()
    {
        transform.position += direction * Time.deltaTime * speed;
    }

    public override void Hit()
    {
        direction = Vector3.right;
        GetComponent<SpriteRenderer>().flipX = true;
    }

}

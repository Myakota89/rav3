﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{
    public GameObject fireBall = default;

    private void Start()
    {
        GetComponent<Animator>().SetTrigger("Attack");
        Destroy(gameObject, 4.5f);
        StartCoroutine(Attack());
        StartCoroutine(MeleeAttack());
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(2.0f);
        Instantiate(fireBall, transform.position + new Vector3(-2, -1), Quaternion.identity);
    }

    IEnumerator MeleeAttack()
    {
        while (true)
        {
            var hit = Physics2D.Raycast(transform.position, Vector2.left, 3.0f);
            if (hit.collider?.GetComponent<Player>())
            {
                GameManager.Instance.PlayPlayerHit();
                hit.collider.GetComponent<Player>().Hit();
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "FireBall")
        {
            Destroy(collision.gameObject);
            GameManager.Instance.DragonHit();
            GetComponent<Animator>().SetTrigger("Hit");

        }
        
    }
}

﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Runner2D : MonoBehaviour {

	public static Runner2D Instance = default;
	public Transform[] points;
	public float speed = 5;

	public GameObject startSectionPrefab;
	public GameObject[] sectionLink;
	private Transform[] section;
	private GameObject sectionStart;
	public GameObject dragonSection;
	public GameObject dragonSectionPrefab;
	private List<Transform> sectionDisabled;
	private float minPosX, addPosX;
	private int index;

	public Transform dragonPoint = default;

	void Start()
	{
		Instance = this;
		minPosX = points[0].position.x;
		addPosX = Mathf.Abs(minPosX) * 3;

		StartGame();
	}

	Transform RandomSection()
	{
		sectionDisabled = new List<Transform>();
		foreach(Transform tr in section)
		{
			if(!tr.gameObject.activeSelf)
			{
				sectionDisabled.Add(tr);
			}
		}
		int rnd = Random.Range(0, sectionDisabled.Count);
		return sectionDisabled[rnd];
	}

	void AddSection()
	{
		Transform bock = default;
		if (GameManager.Instance.isDragon)
        {
			dragonSection = Instantiate(dragonSectionPrefab);
			bock = dragonSection.transform;
		}
			
		else
			bock = RandomSection();
		
		if(index == points.Length) index = 0;
		bock.parent = points[index];
		if (GameManager.Instance.isDragon)
			dragonPoint = points[index];
		bock.localPosition = Vector3.zero;
		bock.gameObject.SetActive(true);
		var resurectionObjects = bock.GetComponentsInChildren<IRessurection>();
		foreach (var obj in resurectionObjects)
			obj.Resurection();

        if (GameManager.Instance.isGodMode)
        {
			var enemys = bock.GetComponentsInChildren<Enemy>();
			foreach (var enemy in enemys)
				enemy.Hit();

		}
		index++;
	}

	void StartGame()
	{
		section = new Transform[sectionLink.Length];

		for(int i = 0; i < sectionLink.Length; i++)
		{
			GameObject clone = Instantiate(sectionLink[i]);
			clone.SetActive(false);
			section[i] = clone.transform;
		}

		sectionStart = Instantiate(startSectionPrefab);
		sectionStart.SetActive(true);
		sectionStart.transform.parent = points[1];
		sectionStart.transform.localPosition = Vector3.zero;

		Transform bock = RandomSection();
		bock.parent = points[0];
		bock.localPosition = Vector3.zero;
		bock.gameObject.SetActive(true);

		bock = RandomSection();
		bock.parent = points[2];
		bock.localPosition = Vector3.zero;
		bock.gameObject.SetActive(true);
	}

	void Update()
	{
		if (dragonPoint != null && dragonPoint.transform.position.x < 0)
			return;

		foreach(Transform tr in points)
		{
			tr.position -= new Vector3(GameManager.Instance.rateSpeed * speed * Time.deltaTime, 0, 0);
			if(tr.position.x < minPosX && dragonPoint == default)
			{
				tr.position += new Vector3(addPosX, 0, 0);
				tr.GetChild(0).gameObject.SetActive(false);
				tr.DetachChildren();
				AddSection();
			}
		}

		if (dragonPoint != default && dragonPoint.position.x < 0)
        {
			GameManager.Instance.StartDragonScene();
			speed = 0;
		}
	}
}

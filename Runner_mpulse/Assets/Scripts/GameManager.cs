﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = default;
    private int coins = default;
    public float rateSpeed = 1;
    public float rateJump = 1;
    public int rateCoins = 1;
    public bool isGodMode = default;
    private bool isPause = default;
    public bool isDragon = default;
    public bool dragonScene = default;
    public Text coinsUI = default;
    public Text menuCoinsUI = default;

    public GameObject loseMenu = default;
    public GameObject winMenu = default;
    public GameObject boosterMenu = default;

    public Player player = default;

    public List<Image> soundValues = default;
    public List<Image> musicValues = default;
    public Sprite activeValueSprite = default;
    public Sprite deactiveValueSprite = default;

    private GameObject music = default;
    public GameObject musicPrefab = default;

    private AudioSource audio = default;
    public AudioClip audioSelect = default;
    public AudioClip audioLose = default;
    public AudioClip audioWin = default;
    public AudioClip audioAttack = default;
    public AudioClip audioEnemyHit = default;
    public AudioClip audioPlayerHit = default;
    public AudioClip audioJump = default;
    public AudioClip audioPickup = default;
    public AudioClip audioDeath = default;
    public AudioClip audioSpin = default;
    public AudioClip audioCheckReward = default;
    public AudioClip audioDragonHit = default;

    public Setting settings = default;

    public List<Transform> pointsAttack = default;
    public GameObject dragonPrefab = default;
    public int dragonHealth = default;
    private void Start()
    {
        Time.timeScale = 1.0f;
        Instance = this;
        music = GameObject.FindGameObjectWithTag("Music");
        if (music == null)
            music = Instantiate(musicPrefab);

        audio = GetComponent<AudioSource>();
        ChangeSoundVolume(settings.soundVolume);
        ChangeMusicVolume(settings.musicVolume);
    }

    private void Update()
    {
        coinsUI.text = coins + "/150";
    }

    public void Pause()
    {
        isPause = !isPause;
        Time.timeScale = isPause ? 0.0f : 1.0f;

        menuCoinsUI.text = $"Dragon Approach: {coins} / 150";
    }

    public void AddCoin(int value)
    {
        coins += value;
    }

    public void StartDragonScene()
    {
        if (dragonScene)
            return;

        dragonScene = true;
        player.SetRightLimit();

        StartCoroutine(DragonAttack());
        IEnumerator DragonAttack()
        {
            var rightLimit = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)).x;

            while (isDragon)
            {
                int rnd = Random.Range(0, pointsAttack.Count);
                var dragon = Instantiate(dragonPrefab, new Vector2(rightLimit - 1,pointsAttack[rnd].position.y), Quaternion.identity);
                yield return new WaitForSeconds(5.0f);
            }
        }
    }

    public void DragonHit()
    {
        PlayDragonHit();
        dragonHealth--;
        if (dragonHealth <= 0)
        {
            dragonHealth = 0;
            Win();
        }
    }

    public void GodModeOff() =>
        isGodMode = false;

    public void RemoveCoin(int value)
    {
        coins -= value;
        if (coins <= 0)
        {
            coins = 0;
            GameOver();
        }
    }

    private void GameOver()
    {
        PlayDeath();
        player.Dead();
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return new WaitForSeconds(2.0f);
            PlayLose();
            loseMenu.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    private void Win()
    {
        PlayWin();
        winMenu.SetActive(true);
        Time.timeScale = 0.0f;
    }

    public void BoosterGame()
    {
        boosterMenu.SetActive(true);
    }

    public void Restart() =>
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    public void ToMainMenu() =>
        SceneManager.LoadScene("Menu");

    public void PlaySelect() =>
        audio.PlayOneShot(audioSelect);

    public void PlayLose() =>
        audio.PlayOneShot(audioLose);

    public void PlayWin() =>
        audio.PlayOneShot(audioWin);

    public void PlayAttack() =>
        audio.PlayOneShot(audioAttack);

    public void PlayEnemyHit() =>
        audio.PlayOneShot(audioEnemyHit);

    public void PlayPlayerHit() =>
        audio.PlayOneShot(audioPlayerHit);

    public void PlayJump() =>
        audio.PlayOneShot(audioJump);

    public void PlayPickup() =>
        audio.PlayOneShot(audioPickup);

    public void PlayDeath() =>
        audio.PlayOneShot(audioDeath);

    public void PlaySpin() =>
        audio.PlayOneShot(audioSpin);
    public void PlayCheckReward() =>
        audio.PlayOneShot(audioCheckReward);

    public void PlayDragonHit() =>
        audio.PlayOneShot(audioDragonHit);


    public void ChangeSoundVolume(float value)
    {
        ResetValues(soundValues);
        int activeValues = GetActiveValue(value);

        for (int i = 0; i < activeValues; i++)
            soundValues[i].sprite = activeValueSprite;

        GetComponent<AudioSource>().volume = value;
        settings.soundVolume = value;
    }

    public void ChangeMusicVolume(float value)
    {
        ResetValues(musicValues);
        int activeValues = GetActiveValue(value);

        for (int i = 0; i < activeValues; i++)
            musicValues[i].sprite = activeValueSprite;

        music.GetComponent<AudioSource>().volume = value;
        settings.musicVolume = value;
    }

    private void ResetValues(List<Image> values)
    {
        foreach (var val in values)
            val.sprite = deactiveValueSprite;
    }

    private int GetActiveValue(float values)
    {
        int result = 0;

        if (values > 0.9f)
            result = 10;
        else if (values > 0.8f)
            result = 9;
        else if (values > 0.7f)
            result = 8;
        else if (values > 0.6f)
            result = 7;
        else if (values > 0.5f)
            result = 6;
        else if (values > 0.4f)
            result = 5;
        else if (values > 0.3f)
            result = 4;
        else if (values > 0.2f)
            result = 3;
        else if (values > 0.1)
            result = 2;
        else if (values > 0)
            result = 1;
        else if (values == 0)
            result = 0;

        return result;
    }
}

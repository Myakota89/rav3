﻿using UnityEngine;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;

namespace Systems
{
    public class DestroyUnitSystem : IEcsRunSystem
    {
        private GameSettings gameSettings = default;
        private EcsFilter<MoveableComponent, HealthComponent, UnitComponent> units = default;
        private EcsFilter<ScoreComponent> scoreComponent = default;

        public void Run()
        {
            foreach (var i in units)
            {
                if (units.Get1(i).transform.position.x > gameSettings.DestroyUnitPositionX)
                {
                    GameObject.DestroyImmediate(units.Get1(i).transform.gameObject);
                    units.GetEntity(i).Destroy();
                }

                else if (units.Get2(i).isDead)
                {
                    units.Get1(i).transform.gameObject.SetActive(false);
                    GameObject.Destroy(units.Get1(i).transform.gameObject, 1.5f);

                    ref var score = ref scoreComponent.Get1(0).score;
                    if (units.Get3(i).unitType == UnitType.EvilUnit)
                        score += gameSettings.EvilUnitScoreValue;
                    else
                    {
                        score += gameSettings.GoodUnitScoreValue;
                        if (score < 0)
                            score = 0;
                    }
                        
                    
                    units.GetEntity(i).Destroy();
                }
            }
        }

    }
}
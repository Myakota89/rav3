﻿using System;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class UnitSizeSystem : IEcsRunSystem
    {
        private UnitSettings settings = default;
        private EcsFilter<MoveableComponent, HealthComponent, UnitComponent> filter = default;

        public void Run()
        {
            foreach (var i in filter)
            {
                if (SizeChangebleUnit(filter.Get3(i).unitType, filter.Get1(i).transform))
                {
                    ref var transform = ref filter.Get1(i).transform;
                    var ratio = filter.Get2(i).health / 100;
                    transform.localScale = new Vector2(ratio, ratio);
                }
            }
        }

        private bool SizeChangebleUnit(UnitType type, Transform transform) =>
            type == UnitType.EvilUnit && transform.localScale.x > settings.MinSizeUnit;
    }
}
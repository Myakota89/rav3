﻿using System;
using System.Linq;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class DamageSystem : IEcsRunSystem
    {
        private GameSettings settings = default;
        private EcsFilter<LightningComponent, ColliderComponent> lightningFilter = default;
        private EcsFilter<UnitComponent, HealthComponent> units = default;
        private EcsFilter<TimeComponent> time = default;


        public void Run()
        {
            ref var colliders = ref lightningFilter.Get2(0).contactColliders;
            ref var delay = ref time.Get1(0).damageDelayTime;
            
            if (colliders.Count == 0 || delay < settings.DamageDelay)
                return;

            foreach (var collider in colliders)
            {
                foreach (var i in units)
                {
                    
                    if (units.Get1(i).collider.Equals(collider))
                        units.Get2(i).health -= GetDamageValue();

                    if (units.Get2(i).health <= 0)
                        units.Get2(i).isDead = true;

                }
            }
            
            delay = 0;
        }

        private float GetDamageValue()
        {
            var lightning = lightningFilter.Get1(0).lightning;
            var doubleLightning = lightningFilter.Get1(0).doubleLightning;

            if (lightning.gameObject.activeSelf && doubleLightning.gameObject.activeSelf)
            {
                var distance = Vector3.Distance(lightning.transform.position, doubleLightning.transform.position);
                return settings.DamageDistanceRate / distance;
            }
            else
                return settings.DamageValue;
        }
    }
}
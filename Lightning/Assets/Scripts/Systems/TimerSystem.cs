﻿using System;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class TimerSystem : IEcsRunSystem
    {
        private EcsFilter<TimeComponent> filter = default;
        
        public void Run()
        {
            filter.Get1(0).spawnTime += Time.deltaTime;
            filter.Get1(0).damageDelayTime += Time.deltaTime;
            filter.Get1(0).gameTime += Time.deltaTime;

            if (filter.Get1(0).gameTime > 1)
            {
                filter.Get1(0).gameTime = 0;
                filter.Get1(0).timer = filter.Get1(0).timer.Add(new TimeSpan(0, 0, -1));
            }
        }
    }
}
﻿using System;
using Components;
using DG.Tweening;
using Leopotam.Ecs;
using ScriptableObjects;
using TMPro;
using UnityEngine;

namespace Systems
{
    public class DamageAnimationSystem : IEcsRunSystem
    {
        private AnimationSettings settings = default;
        private EcsFilter<MoveableComponent, HealthComponent> filter = default;
        
        public void Run()
        {
            foreach (var i in filter)
            {
                ref var healthComponent = ref filter.Get2(i);
                
                if (healthComponent.health < healthComponent.lastValueHealth)
                {
                    var prefab = GameObject.Instantiate(settings.DamagePrefab, filter.Get1(i).transform);
                    prefab.transform.localPosition = new Vector2(0, 0.5f);
                    var damage = healthComponent.lastValueHealth - healthComponent.health;
                    healthComponent.lastValueHealth = healthComponent.health;
                    AnimateView(prefab, (float)Math.Round(damage, 1));
                }
            }
        }

        private void AnimateView(GameObject view, float value)
        {
            
            view.transform.DOLocalMoveY(1, 0.9f);
            var text = view.GetComponent<TextMeshPro>();
            text.text = value.ToString();
            text.DOColor(new Color(1, 1, 1, 0), 1).OnComplete(() =>
            {
                GameObject.Destroy(view);
            });
        }
    }
}
﻿using System;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class MoveableSystem : IEcsRunSystem
    {
        private EcsFilter<MoveableComponent> filter = default;
        
        public void Run()
        {
            foreach (var i in filter)
            {
                ref var speed = ref filter.Get1(i).speed;
                ref var transform = ref filter.Get1(i).transform;
                transform.position += Vector3.right * speed * Time.deltaTime;
            }
        }
    }
} 
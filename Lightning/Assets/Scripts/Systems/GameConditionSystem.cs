﻿using System;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class GameConditionSystem : IEcsRunSystem
    {
        private GameSettings settings = default;
        private EcsFilter<CanvasUiComponent> canvasFilter = default;
        private EcsFilter<TimeComponent> timeEntitys = default;
        
        public void Run()
        {
            if (timeEntitys.Get1(0).timer.TotalSeconds < 1)
            {
                Time.timeScale = 0f;
                canvasFilter.Get1(0).loseMenu.gameObject.SetActive(true);
            }
        }
    }
}
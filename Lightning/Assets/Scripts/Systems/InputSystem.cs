﻿using System;
using System.Linq;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class InputSystem : IEcsRunSystem
    {
        private EcsFilter<LightningComponent, ColliderComponent> filter = default;
        
        public void Run()
        {
            ref var lightning = ref filter.Get1(0).lightning;
            ref var doubleLightning = ref filter.Get1(0).doubleLightning;
            ref var colliders = ref filter.Get2(0).contactColliders;

            if (Input.touchCount > 0)
            {
                lightning.SetActive(true);
                var touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                lightning.transform.position = new Vector3(touchPosition.x, touchPosition.y, 0);
                doubleLightning.SetActive(false);

                var hits = Physics2D.CircleCastAll(lightning.transform.position, 0.5f, Vector2.zero);
                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                        colliders.Add(hit.collider);

                }
                else
                    colliders.Clear();

                if (Input.touchCount == 2)
                {
                    doubleLightning.SetActive(true);
                    var touchPosition2 = Camera.main.ScreenToWorldPoint(Input.touches[1].position);
                    doubleLightning.transform.position = new Vector3(touchPosition2.x, touchPosition2.y, 0);

                    var offsetY = new Vector3(0, 1);
                    
                    var hits2 = Physics2D.LinecastAll(lightning.transform.position , doubleLightning.transform.position );
               
                    if (hits2.Length > 0)
                    {
                        foreach (var hit in hits2)
                            colliders.Add(hit.collider);
                    }
                    else
                        colliders.Clear();
                    
                }

            }
            else
            {
                lightning.SetActive(false);
                doubleLightning.SetActive(false);
                colliders.Clear();
            }
                
        }
    }
}
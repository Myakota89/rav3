﻿using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class SpawnUnitSystem : IEcsRunSystem
    {
        private GameSettings gameSettings = default;
        private UnitSettings unitSettings = default;
        private PrefabsCollection prefabsCollection = default;
        private EcsWorld world = default;
        private EcsFilter<TimeComponent> filter = default;

        public void Run()
        {
            ref var time = ref filter.Get1(0).spawnTime;
            if (time > gameSettings.SpawnInterval)
            {
                time = 0;
                var startUnitPosition = new Vector2(gameSettings.SpawnUnitPositionX, gameSettings.UnitPositionY);
                if (Random.value > gameSettings.ChanceEvilUnitSpawn)
                    GoodUnitSpawn(startUnitPosition);
                else
                    EvilUnitSpawn(startUnitPosition);

            }
        }

        public void GoodUnitSpawn(Vector2 position)
        {
            var unit = GameObject.Instantiate(prefabsCollection.GoodUnitPrefabPrefab, position, Quaternion.identity);

            var unitEntity = world.NewEntity();
            var health = Random.Range(unitSettings.MinHP, unitSettings.MaxHP);
            unitEntity.Get<HealthComponent>().health = health;
            unitEntity.Get<HealthComponent>().lastValueHealth = health;
            unitEntity.Get<MoveableComponent>().transform = unit.transform;
            unitEntity.Get<MoveableComponent>().speed = 100 / health;
            unitEntity.Get<UnitComponent>().unitType = UnitType.GoodUnit;
            unitEntity.Get<UnitComponent>().collider = unit.GetComponent<BoxCollider2D>();
        }

        public void EvilUnitSpawn(Vector2 position)
        {
            var unit = GameObject.Instantiate(prefabsCollection.EvilUnitPrefabPrefab, position, Quaternion.identity);

            var unitEntity = world.NewEntity();
            var health = Random.Range(unitSettings.MinHP, unitSettings.MaxHP);
            unitEntity.Get<HealthComponent>().health = health;
            unitEntity.Get<HealthComponent>().lastValueHealth = health;
            unitEntity.Get<MoveableComponent>().transform = unit.transform;
            unitEntity.Get<MoveableComponent>().speed = 100 / health;
            unitEntity.Get<UnitComponent>().unitType = UnitType.EvilUnit;
            unitEntity.Get<UnitComponent>().collider = unit.GetComponent<BoxCollider2D>();
        }
        
    }
}
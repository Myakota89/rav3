﻿using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using ScriptableObjects;
using UnityEngine;

namespace Systems
{
    public class GameInitSystem : IEcsInitSystem
    {
        private UiEmitter canvas = default;
        private GameSettings settings = default;
        private PrefabsCollection prefabs = default;
        private EcsWorld world = default;
        
        public void Init()
        {
            var timeEntity = world.NewEntity();
            timeEntity.Get<TimeComponent>().timer = settings.GameTimer;

            var lightningPrefab = GameObject.Instantiate(prefabs.Lightning);
            var lightningEntity = world.NewEntity();
            lightningEntity.Get<LightningComponent>().lightning = lightningPrefab;
            lightningEntity.Get<LightningComponent>().doubleLightning = GameObject.Instantiate(prefabs.DoubleLightning);
            lightningEntity.Get<LightningVisualComponent>().lineRenderer = lightningPrefab.GetComponent<LineRenderer>();
            lightningEntity.Get<ColliderComponent>().contactColliders = new HashSet<Collider2D>();

            var scoreEntity = world.NewEntity();
            scoreEntity.Get<ScoreComponent>();

            var canvasEntity = world.NewEntity();
            canvasEntity.Get<CanvasUiComponent>().scoreText = canvas.ScoreText;
            canvasEntity.Get<CanvasUiComponent>().gameTimeText = canvas.GameTimeText;
            canvasEntity.Get<CanvasUiComponent>().recordText = canvas.RecordText;
            canvasEntity.Get<CanvasUiComponent>().loseMenu = canvas.LoseMenu;
            
        }
    }
}
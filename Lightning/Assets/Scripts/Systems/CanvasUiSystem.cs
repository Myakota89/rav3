﻿using Components;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using UnityEngine;

namespace Systems
{
    public class CanvasUiSystem : IEcsRunSystem
    {
        private EcsFilter<CanvasUiComponent> canvasFilter = default;
        private EcsFilter<TimeComponent> timeFilter = default;
        private EcsFilter<ScoreComponent> scoreFilter = default;

        public void Run()
        {
            canvasFilter.Get1(0).scoreText.text = "Score: " + scoreFilter.Get1(0).score.ToString();
            var time = timeFilter.Get1(0).timer;
            var timeText = time.Seconds > 9 ? "00:" + time.Seconds : "00:0" + time.Seconds;
            canvasFilter.Get1(0).gameTimeText.text = timeText;
            canvasFilter.Get1(0).recordText.text = "Best result: " + PlayerPrefs.GetInt("BestScore", 0).ToString();


        }
    }
}
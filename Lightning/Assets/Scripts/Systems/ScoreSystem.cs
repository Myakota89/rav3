﻿using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class ScoreSystem : IEcsRunSystem
    {
        private EcsFilter<ScoreComponent> scoreFilter = default;
        
        public void Run()
        {
            ref var scoreComponent = ref scoreFilter.Get1(0);
            if (scoreComponent.score < 0)
                scoreComponent.score = 0;

            var bestScore = PlayerPrefs.GetInt("BestScore", 0);
            if (scoreComponent.score > bestScore)
                PlayerPrefs.SetInt("BestScore", scoreComponent.score);
        }
    }
}
﻿using Components;
using Leopotam.Ecs;

namespace Systems
{
    public class LightningVisualSystem : IEcsRunSystem
    {
        private EcsFilter<LightningVisualComponent, LightningComponent> filter = default;
        
        public void Run()
        {
            var lightning = filter.Get2(0);
            ref var line = ref filter.Get1(0).lineRenderer;

            if (lightning.lightning.activeSelf && lightning.doubleLightning.activeSelf)
            {
                line.positionCount = 2;
                line.SetPosition(0, lightning.lightning.transform.position);
                line.SetPosition(1, lightning.doubleLightning.transform.position);
            }
            else
                line.positionCount = 0;
        }
    }
}
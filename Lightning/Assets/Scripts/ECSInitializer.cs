﻿using System;
using Systems;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using ScriptableObjects;
using UnityEngine;

public class ECSInitializer : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings = default;
    [SerializeField] private UnitSettings unitSettings = default;
    [SerializeField] private PrefabsCollection prefabsCollection = default;
    [SerializeField] private AnimationSettings animSettings = default;
    [SerializeField] private UiEmitter canvas = default;
    
    private EcsWorld world = default;
    private EcsSystems systems = default;

    private void Start()
    {
        world = new EcsWorld();
        systems = new EcsSystems(world);
        

        systems.Add(new TimerSystem())
            .Add(new GameInitSystem())
            .Add(new SpawnUnitSystem())
            .Add(new MoveableSystem())
            .Add(new InputSystem())
            .Add(new DamageSystem())
            .Add(new UnitSizeSystem())
            .Add(new DestroyUnitSystem())
            .Add(new DamageAnimationSystem())
            .Add(new LightningVisualSystem())
            .Add(new ScoreSystem())
            .Add(new GameConditionSystem())
            .Add(new CanvasUiSystem())
            .Inject(gameSettings)
            .Inject(animSettings)
            .Inject(unitSettings)
            .Inject(prefabsCollection)
            .Inject(canvas)
            .Init();

    }

    private void Update()
    {
        systems.Run();
    }

    private void OnDestroy()
    {
        systems.Destroy();
        world.Destroy();
    }
}

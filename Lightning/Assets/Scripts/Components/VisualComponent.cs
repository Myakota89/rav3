﻿using UnityEngine;

namespace Components
{
    public struct VisualComponent
    {
        public Animator animator;
    }
}
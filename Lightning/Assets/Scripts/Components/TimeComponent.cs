﻿using System;

namespace Components
{
    public struct TimeComponent
    {
        public float spawnTime;
        public float damageDelayTime;
        public float gameTime;
        public TimeSpan timer;
    }
}
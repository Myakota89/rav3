﻿namespace Components
{
    public struct HealthComponent
    {
        public float health;
        public float lastValueHealth;
        public bool isDead;
    }
}
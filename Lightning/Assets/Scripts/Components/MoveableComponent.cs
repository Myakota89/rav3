﻿using UnityEngine;

namespace Components
{
    public struct MoveableComponent
    {
        public Transform transform;
        public float speed;
    }
}
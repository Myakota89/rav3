﻿using UnityEngine;

namespace Components
{
    public struct UnitComponent
    {
        public UnitType unitType;
        public Collider2D collider;
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace Components
{
    public struct CanvasUiComponent
    {
        public Text scoreText;
        public Text gameTimeText;
        public Text recordText;
        public Transform loseMenu;
    }
}
﻿using UnityEngine;

namespace Components
{
    public struct LightningComponent
    {
        public GameObject lightning;
        public GameObject doubleLightning;
    }
}
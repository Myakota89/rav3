﻿using UnityEngine;

namespace Components
{
    public struct LightningVisualComponent
    {
        public LineRenderer lineRenderer;
    }
}
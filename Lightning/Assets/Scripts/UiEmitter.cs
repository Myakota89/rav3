﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Systems
{
    public class UiEmitter : MonoBehaviour
    {
        [SerializeField] private Text scoreText = default;
        [SerializeField] private Text gameTimeText = default;
        [SerializeField] private Text recordText = default;
        [SerializeField] private Transform loseMenu = default;
        [SerializeField] private Transform pauseMenu = default;
        private bool isPause = default;

        public Text ScoreText => scoreText;
        public Text GameTimeText => gameTimeText;
        public Text RecordText => recordText;
        public Transform LoseMenu => loseMenu;

        private void Update()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                    ToMainMenu();

            }
        }

        public void Pause()
        {
            isPause = !isPause;

            if (isPause)
            {
                pauseMenu.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                pauseMenu.gameObject.SetActive(false);
                Time.timeScale = 1;
            }
        }


        public void RestartGame() =>
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        public void ToMainMenu() =>
            SceneManager.LoadScene("Menu");

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
                Pause();
        }
    }
}
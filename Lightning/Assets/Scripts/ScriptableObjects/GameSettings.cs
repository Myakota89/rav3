﻿using System;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/GameSettings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeField] private float spawnUnitPositionX = default;
        [SerializeField] private float destroyUnitPositionX = default;
        [SerializeField] private float unitPositionY = default;
        [SerializeField] private float spawnInterval = default;
        [SerializeField] private float damageDelay = default;
        [SerializeField] private float damageValue = default;
        [SerializeField] private float damageDistanceRate = default;
        [SerializeField] private int evilUnitScoreValue = default;
        [SerializeField] private int goodUnitScoreValue = default;
        [SerializeField] private int gameTimeSeconds = default;
        [SerializeField] [Range(0, 100)] private float chanceEvilUnitSpawn = default;

        public float SpawnUnitPositionX => spawnUnitPositionX;
        public float DestroyUnitPositionX => destroyUnitPositionX;
        public float UnitPositionY => unitPositionY;
        public float SpawnInterval => spawnInterval;
        public float ChanceEvilUnitSpawn => chanceEvilUnitSpawn / 100;
        public float DamageDelay => damageDelay;
        public float DamageValue => damageValue;
        public float DamageDistanceRate => damageDistanceRate;
        public int EvilUnitScoreValue => evilUnitScoreValue;
        public int GoodUnitScoreValue => goodUnitScoreValue;
        public TimeSpan GameTimer => new TimeSpan(0, 0, gameTimeSeconds);
    }
}
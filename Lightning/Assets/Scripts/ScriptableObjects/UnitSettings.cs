﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Unit Settings")]
    public class UnitSettings : ScriptableObject
    {
        [SerializeField] private float minHP = default;
        [SerializeField] private float maxHP = default;
        [SerializeField] private float minSizeUnit = default;

        public float MinHP => minHP;
        public float MaxHP => maxHP;
        public float MinSizeUnit => minSizeUnit;
    }
}
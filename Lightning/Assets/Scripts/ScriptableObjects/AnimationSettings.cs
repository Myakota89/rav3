﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/AnimationSettings")]
    public class AnimationSettings : ScriptableObject
    {
        [SerializeField] private GameObject damagePrefab = default;

        public GameObject DamagePrefab => damagePrefab;
    }
}
﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Settings/Prefabs Collection")]
    public class PrefabsCollection : ScriptableObject
    {
        [SerializeField] private GameObject goodUnitPrefab = default;
        [SerializeField] private GameObject evilUnitPrefab = default;
        [SerializeField] private GameObject lightning = default;
        [SerializeField] private GameObject doubleLightning = default;

        public GameObject GoodUnitPrefabPrefab => goodUnitPrefab;
        public GameObject EvilUnitPrefabPrefab => evilUnitPrefab;
        public GameObject Lightning => lightning;
        public GameObject DoubleLightning => doubleLightning;
    }
}